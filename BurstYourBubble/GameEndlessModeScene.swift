//
//  GameEndlessModeScene.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/3/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class GameEndlessModeScene: GameScene {
   
    var scoreLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultLight)
    
    var gameLeftTime:Int = InitialTime_Endless
    var elapsedGameTime:Int = 0
    var timer:NSTimer = NSTimer()
    var timeLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultLight)
    var counterLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultLight)
    
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    override func initAttributes() {
        super.initAttributes()
        
        gameMode = GameMode.Endless
    }
    
    override func initUIs() {
        // Score Label
        scoreLabel.position = CGPointMake(-80, self.frame.size.height - 25)
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        scoreLabel.name = "scoreLabel"
        scoreLabel.fontSize = 15
        scoreLabel.fontColor = Color_Gray
        scoreLabel.zPosition = -1
        gameLayer.addChild(scoreLabel)
        
        // Time Label
        timeLabel.position = CGPointMake(self.frame.size.width + 20, self.frame.size.height - 25)
        timeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        timeLabel.name = "timeLabel"
        timeLabel.fontSize = 15
        timeLabel.fontColor = Color_Gray
        timeLabel.zPosition = -1
        timeLabel.text = "Time:"
        gameLayer.addChild(timeLabel)
        
        counterLabel.position = CGPointMake(timeLabel.position.x + 35, timeLabel.position.y)
        counterLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        counterLabel.name = "counterLabel"
        counterLabel.fontSize = 15
        counterLabel.fontColor = Color_Gray
        counterLabel.zPosition = -1
        gameLayer.addChild(counterLabel)
        
        super.initUIs()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateUIs(currentTime: CFTimeInterval) {
        super.updateUIs(currentTime)
        
        scoreLabel.text = "Score: " + String(scoreCalculator.totalScore)
        counterLabel.text = String(gameLeftTime)
    }
    
    override func updateGameOverCondition() {
        // Switch to GameOverScene if time is up OR all the bubbles have gone
        if (gameLeftTime <= 0 || bubbleCount == 0) {
            gameEnd()
        }
    }
    
    override func gameStart() {
        super.gameStart()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "tick", userInfo: nil, repeats: true)
        
        // Move labels
        scoreLabel.runAction(SKAction.sequence([
            SKAction.moveByX(100, y: 0, duration: 0.2),
            SKAction.moveByX(-10, y: 0, duration: 0.1),
            SKAction.moveByX(7, y: 0, duration: 0.05),
            SKAction.moveByX(-2, y: 0, duration: 0.02)
        ]))
        
        timeLabel.runAction(SKAction.sequence([
            SKAction.moveByX(-100, y: 0, duration: 0.2),
            SKAction.moveByX(10, y: 0, duration: 0.1),
            SKAction.moveByX(-7, y: 0, duration: 0.05),
            SKAction.moveByX(2, y: 0, duration: 0.02)
        ]))
        
        counterLabel.runAction(SKAction.sequence([
            SKAction.moveByX(-100, y: 0, duration: 0.2),
            SKAction.moveByX(10, y: 0, duration: 0.1),
            SKAction.moveByX(-7, y: 0, duration: 0.05),
            SKAction.moveByX(2, y: 0, duration: 0.02)
        ]))
    }
    
    override func gameEnd() {
        timer.invalidate()
        
        super.gameEnd()
    }
    
    override func gameRestart() {
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                var gameScene:GameEndlessModeScene = GameEndlessModeScene(size: self.size)
                self.view!.presentScene(gameScene, transition: transition)
            })
        ]))
    }
    
    func tick() {
        if (!gameLayer.paused) {
            elapsedGameTime++
            gameLeftTime = InitialTime_Endless - elapsedGameTime
            
            if (gameLeftTime <= 10) {
                if (userSettings.sfxEnabled) {
                    self.runAction(timerSfxAction)
                }
                
                counterLabel.runAction(
                    SKAction.sequence([
                        SKAction.scaleTo(1.3, duration: 0.2),
                        SKAction.scaleTo(1.0, duration: 0.2),
                    ]),
                    withKey: "counterScale"
                )
            }
        }
    }
    
    func addClockIcon(bonusTime: Int, bonusTimeMultiplier: Int, position: CGPoint) {
        //let bonusTime = (1 + numOfBubbles) * numOfBubbles / 2
        var clock = Clock(
            time: bonusTime,
            multiplier: bonusTimeMultiplier,
            initPosition: position,
            destPosition: self.counterLabel.position
        )
        gameLayer.addChild(clock)
    }
    
    func addTimeLeft(time: Int) {
        elapsedGameTime -= time
        gameLeftTime = InitialTime_Endless - elapsedGameTime
        
        counterLabel.fontColor = Color_Green
        counterLabel.fontName = FontName_DefaultBold
        counterLabel.runAction(
            SKAction.sequence([
                SKAction.scaleTo(1.3, duration: 0.15),
                SKAction.scaleTo(1.0, duration: 0.15),
            ]),
            completion: {
                self.counterLabel.fontColor = Color_Gray
                self.counterLabel.fontName = FontName_DefaultLight
            }
        )
    }
}
