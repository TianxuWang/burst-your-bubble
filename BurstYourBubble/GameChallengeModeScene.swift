//
//  GameChallengeModeScene.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/1/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit
import UIKit

class GameChallengeModeScene: GameScene {
    
    var roundNum:Int = 1
    var roundLabel:SKLabelNode!
    
    var roundGoal:Int = InitialGoal_Challenge
    var roundGoalLabel:SKLabelNode!
    var roundGoalCounterLabel:SKLabelNode!
    
    var roundTime:Int = InitialTime_Challenge
    var roundTimeLabel:SKLabelNode!
    var roundTimeCounterLabel:SKLabelNode!
    
    var roundCompletePercent:CGFloat!
    var percentLabel:SKLabelNode!
    
    // Used for timer and label
    var gameLeftTime:Int = InitialTime_Challenge
    var elapsedGameTime:Int = 0
    var timer:NSTimer = NSTimer()
    var timeLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultLight)
    var counterLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultLight)
    
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    override func initAttributes() {
        super.initAttributes()
        
        gameMode = GameMode.Challenge
    }
    
    override func initUIs() {
        percentLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        percentLabel.position = CGPointMake(-80, self.frame.size.height - 25)
        percentLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        percentLabel.name = "percentLabel"
        percentLabel.fontSize = 15
        percentLabel.fontColor = Color_Gray
        percentLabel.zPosition = -1
        gameLayer.addChild(percentLabel)
        
        // Time Label
        timeLabel.position = CGPointMake(self.frame.size.width + 20, self.frame.size.height - 25)
        timeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        timeLabel.name = "timeLabel"
        timeLabel.fontSize = 15
        timeLabel.fontColor = Color_Gray
        timeLabel.zPosition = -1
        timeLabel.text = "Time:"
        gameLayer.addChild(timeLabel)
        
        counterLabel.position = CGPointMake(timeLabel.position.x + 40, timeLabel.position.y)
        counterLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        counterLabel.name = "counterLabel"
        counterLabel.fontSize = 15
        counterLabel.fontColor = Color_Gray
        counterLabel.zPosition = -1
        gameLayer.addChild(counterLabel)
        
        super.initUIs()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateUIs(currentTime: CFTimeInterval) {
        super.updateUIs(currentTime)
        
        counterLabel.text = String(gameLeftTime)
        
        roundCompletePercent = CGFloat(scoreCalculator.totalScore) / CGFloat(roundGoal)
        percentLabel.text = "\(Int(round(roundCompletePercent * 100)))%"
    }
    
    override func updateGameOverCondition() {
        // Switch to GameOverScene if time is up OR all the bubbles have gone
        if (gameLeftTime <= 0 || bubbleCount == 0) {
            gameEnd()
        }
        
        // Head to next round if hit the goal
        if (scoreCalculator.totalScore >= roundGoal) {
            roundNum++
            
            timer.invalidate()
            scoreCalculator.totalScore = 0
            
            moveOutLabels()
            // Show complish
            gameStart()
        }
    }
    
    override func gameStart() {
        // hide ad banner
        NSNotificationCenter.defaultCenter().postNotificationName("hideAdBanner", object: nil)
        pauseButton.hidden = false
        gameStarted = false
        
        self.physicsWorld.speed = 0.0
        for obj in gameLayer.children {
            if (obj is Bubble) {
                var bubble:Bubble = obj as! Bubble
                bubble.userInteractionEnabled = false
            }
        }
        
        bgMusicPlayer.pause()
        
        if (roundNum > 1) {
            if (userSettings.sfxEnabled) {
                self.runAction(successSfxAction)
            }
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(1.0)
                ]), completion: {
                    self.initRoundLabel()
                }
            )
        }
        else {
            initRoundLabel()
        }
    }
    
    override func gameEnd() {
        gameStarted = false
        timer.invalidate()
        
        if (userSettings.sfxEnabled) {
            drumWhirlingSfxPlayer.stop()
            self.runAction(drumBadSfxAction)
        }
        
        scoreCalculator.roundNum = self.roundNum
        var record:Int? = self.scoreCalculator.saveScore()
        record = record == nil ? 0 : record!
        
        // Switch to GameOverScene
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 1.5)
                var gameOverScene:SKScene = GameOverScene(size: self.size, score: self.roundNum, record: record!)
                self.view!.presentScene(gameOverScene, transition: transition)
                bgMusicPlayer.pause()
            })
        ]))
    }
    
    override func gameRestart() {
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                var gameScene:GameChallengeModeScene = GameChallengeModeScene(size: self.size)
                self.view!.presentScene(gameScene, transition: transition)
            })
        ]))
    }
    
    func initRoundLabel() {
        if (roundLabel != nil) {
            roundLabel.removeFromParent()
        }
        roundLabel = SKLabelNode(fontNamed: FontName_SymbolHeavy)
        roundLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 15)
        roundLabel.name = "roundLabel"
        roundLabel.fontSize = 90
        roundLabel.fontColor = userSettings.bgColorBlack ? Color_White : Color_Black
        roundLabel.text = "ROUND \(roundNum)"
        roundLabel.alpha = 0
        gameLayer.addChild(roundLabel)
        
        roundLabel.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.fadeAlphaTo(1.0, duration: 0.2),
                SKAction.scaleTo(0.5, duration: 0.2)
            ]),
            SKAction.runBlock({
                if (userSettings.sfxEnabled) {
                    drumWhirlingSfxPlayer.stop()
                    self.runAction(drumGoodSfxAction)
                }
            }),
            SKAction.waitForDuration(1.0),
            SKAction.group([
                SKAction.scaleTo(0.15, duration: 0.2),
                SKAction.moveToY(self.frame.height - 25, duration: 0.2)
            ])]),
            completion: {
                self.roundLabel.fontSize = 15
                self.roundLabel.fontColor = Color_Gray
                self.roundLabel.setScale(1.0)
                self.initRoundGoalLabel()
            }
        )
    }
    
    func initRoundGoalLabel() {
        roundGoalLabel = SKLabelNode(fontNamed: FontName_SymbolHeavy)
        roundGoalLabel.position = CGPointMake(-100, self.frame.size.height / 2 + 10)
        roundGoalLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        roundGoalLabel.name = "roundGoalLabel"
        roundGoalLabel.fontSize = 25
        roundGoalLabel.fontColor = userSettings.bgColorBlack ? Color_White : Color_Black
        roundGoalLabel.text = "GOAL:"
        roundGoalLabel.alpha = 0
        gameLayer.addChild(roundGoalLabel)
        
        roundGoalCounterLabel = SKLabelNode(fontNamed: FontName_SymbolHeavy)
        roundGoalCounterLabel.position = CGPointMake(90, 0)
        roundGoalCounterLabel.name = "roundGoalCounterLabel"
        roundGoalCounterLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        roundGoalCounterLabel.fontSize = 25
        roundGoalCounterLabel.fontColor = roundGoalLabel.fontColor
        roundGoalCounterLabel.text = "\(roundGoal)pts"
        roundGoalLabel.addChild(roundGoalCounterLabel)
        
        roundGoalLabel.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.moveByX(self.frame.size.width / 2, y: 0, duration: 0.2),
                SKAction.fadeInWithDuration(0.2)
            ]),
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                if (self.roundNum > 1) {
                    self.roundGoalCounterLabel.runAction(SKAction.sequence([
                        SKAction.scaleTo(1.3, duration: 0.1),
                        SKAction.runBlock({
                            self.updateGoalByRoundNum()
                            self.roundGoalCounterLabel.text = "\(self.roundGoal)pts"
                            if (userSettings.sfxEnabled) {
                                self.runAction(updateSfxAction)
                            }
                        }),
                        SKAction.waitForDuration(0.1),
                        SKAction.scaleTo(1.0, duration: 0.2),
                        SKAction.waitForDuration(1.5)
                    ]))
                }
            }),
            SKAction.waitForDuration(self.roundNum > 1 ? 2.0 : 0),
            SKAction.fadeOutWithDuration(0.5),
            SKAction.removeFromParent()
        ]))
        
        roundTimeLabel = SKLabelNode(fontNamed: FontName_SymbolHeavy)
        roundTimeLabel.position = CGPointMake(self.frame.size.width, self.frame.size.height / 2 - 30)
        roundTimeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        roundTimeLabel.name = "roundTimeLabel"
        roundTimeLabel.fontSize = 25
        roundTimeLabel.fontColor = roundGoalLabel.fontColor
        roundTimeLabel.text = "TIME:"
        roundTimeLabel.alpha = 0
        gameLayer.addChild(roundTimeLabel)
        
        roundTimeCounterLabel = SKLabelNode(fontNamed: FontName_SymbolHeavy)
        roundTimeCounterLabel.position = CGPointMake(81, 0)
        roundTimeCounterLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        roundTimeCounterLabel.name = "roundTimeCounterLabel"
        roundTimeCounterLabel.fontSize = 25
        roundTimeCounterLabel.fontColor = roundGoalLabel.fontColor
        roundTimeCounterLabel.text = "\(gameLeftTime)s"
        roundTimeLabel.addChild(roundTimeCounterLabel)
        
        roundTimeLabel.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.moveByX(-self.frame.size.width / 2 - 91, y: 0, duration: 0.2),
                SKAction.fadeInWithDuration(0.2)
            ]),
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                if (self.roundNum > 1) {
                    self.roundTimeCounterLabel.runAction(SKAction.sequence([
                        SKAction.waitForDuration(1.0),
                        SKAction.scaleTo(1.3, duration: 0.1),
                        SKAction.runBlock({
                            self.updateTimeByRoundNum()
                            self.roundTimeCounterLabel.text = "\(self.roundTime)s"
                            if (userSettings.sfxEnabled) {
                                self.runAction(updateSfxAction)
                            }
                        }),
                        SKAction.waitForDuration(0.1),
                        SKAction.scaleTo(1.0, duration: 0.2)
                    ]))
                }
            }),
            SKAction.waitForDuration(self.roundNum > 1 ? 2.0 : 0),
            SKAction.fadeOutWithDuration(0.5),
            SKAction.removeFromParent()
            ]),
            completion: {
                self.roundStart()
            }
        )
    }
    
    func roundStart() {
        gameStarted = true

        ViewController.playTrackAtIndex(userSettings.selectedMusicThemeId)
        bgMusicPlayer.play()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "tick", userInfo: nil, repeats: true)
        moveInLabels()
        
        self.physicsWorld.speed = 1.0
        for obj in gameLayer.children {
            if (obj is Bubble) {
                var bubble:Bubble = obj as! Bubble
                bubble.userInteractionEnabled = true
                bubble.applyRandomImpulse()
            }
        }
    }
    
    override func gamePause() {
        super.gamePause()
        
        drumWhirlingSfxPlayer.pause()
    }
    
    func updateGoalByRoundNum() {
        roundGoal = (roundNum - 1) * 500 * roundNum / 2
    }
    
    func updateTimeByRoundNum() {
        elapsedGameTime = 0
        roundTime = 10 + gameLeftTime
    }
    
    func tick() {
        if (!gameLayer.paused) {
            elapsedGameTime++
            gameLeftTime = roundTime - elapsedGameTime
            
            if (gameLeftTime <= 10) {
                if (userSettings.sfxEnabled) {
                    self.runAction(timerSfxAction)
                    if (gameLeftTime <= 4 && !drumWhirlingSfxPlayer.playing) {
                        drumWhirlingSfxPlayer.play()
                    }
                }
                
                counterLabel.runAction(
                    SKAction.sequence([
                        SKAction.scaleTo(1.3, duration: 0.1),
                        SKAction.scaleTo(1.0, duration: 0.2),
                        ]),
                    withKey: "counterScale"
                )
            }
        }
    }
    
    func moveInLabels() {
        percentLabel.runAction(SKAction.sequence([
            SKAction.moveByX(100, y: 0, duration: 0.2),
            SKAction.moveByX(-10, y: 0, duration: 0.1),
            SKAction.moveByX(7, y: 0, duration: 0.05),
            SKAction.moveByX(-2, y: 0, duration: 0.02)
            ]))
        
        timeLabel.runAction(SKAction.sequence([
            SKAction.moveByX(-100, y: 0, duration: 0.2),
            SKAction.moveByX(10, y: 0, duration: 0.1),
            SKAction.moveByX(-7, y: 0, duration: 0.05),
            SKAction.moveByX(2, y: 0, duration: 0.02)
            ]))
        
        counterLabel.runAction(SKAction.sequence([
            SKAction.moveByX(-100, y: 0, duration: 0.2),
            SKAction.moveByX(10, y: 0, duration: 0.1),
            SKAction.moveByX(-7, y: 0, duration: 0.05),
            SKAction.moveByX(2, y: 0, duration: 0.02)
            ]))
    }
    
    func moveOutLabels() {
        percentLabel.runAction(SKAction.sequence([
            SKAction.moveByX(-100, y: 0, duration: 0.2),
            SKAction.moveByX(10, y: 0, duration: 0.1),
            SKAction.moveByX(-7, y: 0, duration: 0.05),
            SKAction.moveByX(2, y: 0, duration: 0.02)
            ]))
        
        timeLabel.runAction(SKAction.sequence([
            SKAction.moveByX(100, y: 0, duration: 0.2),
            SKAction.moveByX(-10, y: 0, duration: 0.1),
            SKAction.moveByX(7, y: 0, duration: 0.05),
            SKAction.moveByX(-2, y: 0, duration: 0.02)
            ]))
        
        counterLabel.runAction(SKAction.sequence([
            SKAction.moveByX(100, y: 0, duration: 0.2),
            SKAction.moveByX(-10, y: 0, duration: 0.1),
            SKAction.moveByX(7, y: 0, duration: 0.05),
            SKAction.moveByX(-2, y: 0, duration: 0.02)
            ]))
    }
}
