//
//  FloatText.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/19/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

let MaxFontSize:CGFloat = 42

class FloatText: SKLabelNode {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(fontNamed fontName: String!) {
        super.init(fontNamed: fontName)
    }
    
    override init() {
        super.init()
    }
    
    init(text:String, position:CGPoint, fontName:String, fontSize:CGFloat, fontColor:SKColor) {
        super.init(fontNamed: fontName)
        
        self.name = "floatText"
        self.position = position
        self.fontSize = fontSize
        self.fontColor = fontColor
        self.text = text
        self.alpha = 0
        self.zPosition = 3
        self.setScale(0.1)
        
        self.runAction(SKAction.group([
            SKAction.sequence([
                SKAction.scaleTo(1.3, duration: 0.1),
                SKAction.scaleTo(0.8, duration: 0.05),
                SKAction.scaleTo(1.0, duration: 0.02)
                ]),
            SKAction.moveBy(CGVectorMake(0, 10), duration: 1),
            SKAction.sequence([
                SKAction.fadeInWithDuration(0.1),
                SKAction.waitForDuration(0.5),
                SKAction.fadeOutWithDuration(0.5)
                ])
            ]),
            completion: {
                self.removeFromParent()
            }
        )
    }
    
    convenience init(text:String, position:CGPoint, level:Int, color:BubbleColor) {
        self.init(
            text: text,
            position: position,
            fontName: FontName_DefaultBold,
            fontSize: min(CGFloat(15 + level), MaxFontSize),
            fontColor: color.toSKColor()
        )
    }
    
    func changeColor() {
        var color = BubbleColor.randomColor()
        self.runAction(SKAction.repeatActionForever(SKAction.sequence([
            SKAction.runBlock({
                self.fontColor = color.toSKColor()
                color = BubbleColor.getNextColor(color)
            }),
            SKAction.waitForDuration(0.1),
        ])), withKey: "changeColor")
    }
}
