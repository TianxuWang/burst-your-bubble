//
//  AppDelegate.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/3/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import UIKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: AVAudioSessionCategoryOptions.MixWithOthers, error: nil)
        AVAudioSession.sharedInstance().setMode(AVAudioSessionModeDefault, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        
        if (NSUserDefaults.standardUserDefaults().boolForKey("hasLaunchedOnce")) {
            // This is not the first launch of the app
            isFirstLaunch = false
        }
        else {
            // This is the first launch of the app
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "hasLaunchedOnce")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            isFirstLaunch = true
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        if (bgMusicPlayer != nil && userSettings != nil) {
            userSettings.bgMusicEnabled = userSettings.bgMusicEnabled && !AVAudioSession.sharedInstance().otherAudioPlaying
            bgMusicPlayer.volume = userSettings.bgMusicEnabled ? 1.0 : 0.0
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NSNotificationCenter.defaultCenter().postNotificationName("pauseApp", object: nil)
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

