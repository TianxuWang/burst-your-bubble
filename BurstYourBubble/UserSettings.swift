//
//  UserSettings.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/21/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import Foundation

class UserSettings: NSObject, NSCoding {
    
    var bgMusicEnabled:Bool
    var sfxEnabled:Bool
    var vibrationEnabled:Bool
    var bgColorBlack:Bool
    var selectedMusicThemeId:Int
    var selectedPaletteId:Int
    var musicThemeUnlock:[Bool]
    var paletteUnlock:[Bool]
    var achievementsTrack:[Int]
    
    override init() {
        self.bgMusicEnabled = true
        self.sfxEnabled = true
        self.vibrationEnabled = true
        self.bgColorBlack = false
        self.selectedMusicThemeId = 0
        self.selectedPaletteId = 0
        self.musicThemeUnlock = Utility.initUnlockArray(SoundThemeNum)
        self.paletteUnlock = Utility.initUnlockArray(PaletteNum)
        self.achievementsTrack = [Int](count: AchievementTrackedNum, repeatedValue: 0)
    }
    
    required init(coder aDecoder: NSCoder) {
        self.bgMusicEnabled = aDecoder.decodeObjectForKey("bgMusicEnabled") as! Bool
        self.sfxEnabled = aDecoder.decodeObjectForKey("sfxEnabled") as! Bool
        self.vibrationEnabled = aDecoder.decodeObjectForKey("vibrationEnabled") as! Bool
        self.bgColorBlack = aDecoder.decodeObjectForKey("bgColorBlack") as! Bool
        self.selectedMusicThemeId = aDecoder.decodeObjectForKey("selectedMusicThemeId") as! Int
        self.selectedPaletteId = aDecoder.decodeObjectForKey("selectedPaletteId") as! Int
        self.musicThemeUnlock = aDecoder.decodeObjectForKey("musicThemeUnlock") as! [Bool]
        self.paletteUnlock = aDecoder.decodeObjectForKey("paletteUnlock") as! [Bool]
        self.achievementsTrack = aDecoder.decodeObjectForKey("achievementTrack") as! [Int]
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.bgMusicEnabled, forKey: "bgMusicEnabled")
        aCoder.encodeObject(self.sfxEnabled, forKey: "sfxEnabled")
        aCoder.encodeObject(self.vibrationEnabled, forKey: "vibrationEnabled")
        aCoder.encodeObject(self.selectedMusicThemeId, forKey: "selectedMusicThemeId")
        aCoder.encodeObject(self.selectedPaletteId, forKey: "selectedPaletteId")
        aCoder.encodeObject(self.bgColorBlack, forKey: "bgColorBlack")
        aCoder.encodeObject(self.musicThemeUnlock, forKey: "musicThemeUnlock")
        aCoder.encodeObject(self.paletteUnlock, forKey: "paletteUnlock")
        aCoder.encodeObject(self.achievementsTrack, forKey: "achievementTrack")
    }
}
