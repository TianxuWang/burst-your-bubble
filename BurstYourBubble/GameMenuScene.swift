//
//  GameMenuScene.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/30/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import UIKit
import SpriteKit

class GameMenuScene: SKScene {
   
    let startLabelSizeChangeRange:CGFloat = 0.1
    let startLabelSizeChangeSpeed = 5.0
    
    var titleLabel1:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var titleLabel2:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var gameCenterBubble:ColorBubble!
    var gameCenterIcon:SKSpriteNode!
    var settingBubble:ColorBubble!
    var settingIcon:SKSpriteNode!
    var customBubble:ColorBubble!
    var customIcon:SKSpriteNode!
    var moneyLabel:SKLabelNode!
    
    var instructionBubble:ColorBubble!
    var instructionLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_Symbol)
    var infoBubble:ColorBubble!
    var infoLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_Info)
    
    var startBubble:ColorBubble!
    var startLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    
    var timeModeBubble:ColorBubble!
    var timeModeLabel:SKLabelNode!
    var challengeModeBubble:ColorBubble!
    var challengeModeLabel:SKLabelNode!
    var endlessModeBubble:ColorBubble!
    var endlessModeLabel:SKLabelNode!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        gameMode = nil
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        
        // Game Title
        titleLabel1.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 165)
        titleLabel1.name = "titleLabel1"
        titleLabel1.fontSize = 30
        titleLabel1.fontColor = Color_Gray
        titleLabel1.text = "Burst Your"
        self.addChild(titleLabel1)
        
        titleLabel2.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 95)
        titleLabel2.name = "titleLabel2"
        titleLabel2.fontSize = 50
        titleLabel2.fontColor = Color_Gray
        titleLabel2.text = "BUBBLE"
        self.addChild(titleLabel2)
        
        // Start Label
        startLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 12)
        startLabel.name = "startLabel"
        startLabel.fontSize = 35
        startLabel.zPosition = 1
        startLabel.text = "Play"
        startLabel.setScale(0.05)
        self.addChild(startLabel)
        startLabel.alpha = 0
        startLabel.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.fadeInWithDuration(0.2)
        ]))
        
        // Initialize the first bubble
        startBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            0,                                                                          // bubble level (0~9): detemine size and speed
            position: CGPointMake(self.frame.size.width/2, self.frame.size.height/2)    // initial position of bubble
        )
        startBubble.name = "startBubble"
        startBubble.userInteractionEnabled = false
        startBubble.physicsBody!.dynamic = false
        self.addChild(startBubble)
        
        // Initialize UI buttons and labels
        gameCenterBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width/2 - 70, self.frame.size.height/2 - 120)
        )
        gameCenterBubble.name = "gameCenterBubble"
        gameCenterBubble.physicsBody!.dynamic = false
        gameCenterBubble.userInteractionEnabled = false
        self.addChild(gameCenterBubble)
        
        gameCenterIcon = SKSpriteNode(imageNamed: "trophy.png")
        gameCenterIcon.name = "gameCenterIcon"
        gameCenterIcon.position = CGPointMake(self.frame.size.width/2 - 70, self.frame.size.height/2 - 120)
        gameCenterIcon.setScale(0.01)
        self.addChild(gameCenterIcon)
        gameCenterIcon.alpha = 0
        gameCenterIcon.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(0.09, duration: 0.2),
                    SKAction.scaleTo(0.07, duration: 0.02)
                ])
            ])
        ]))
        
        settingBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 120)
        )
        settingBubble.name = "settingBubble"
        settingBubble.physicsBody!.dynamic = false
        settingBubble.userInteractionEnabled = false
        self.addChild(settingBubble)
        
        settingIcon = SKSpriteNode(imageNamed: "gear.png")
        settingIcon.name = "settingIcon"
        settingIcon.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 120)
        settingIcon.setScale(0.05)
        settingIcon.alpha = 0
        self.addChild(settingIcon)
        settingIcon.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(0.4, duration: 0.2),
                    SKAction.scaleTo(0.32, duration: 0.02)
                ])
            ])
        ]))
        
        customBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width/2 + 70, self.frame.size.height/2 - 120)
        )
        customBubble.name = "customBubble"
        customBubble.physicsBody!.dynamic = false
        customBubble.userInteractionEnabled = false
        self.addChild(customBubble)
        
        customIcon = SKSpriteNode(imageNamed: "magicwand.png")
        customIcon.name = "customIcon"
        customIcon.position = CGPointMake(self.frame.size.width/2 + 70, self.frame.size.height/2 - 120)
        customIcon.setScale(0.05)
        customIcon.alpha = 0
        self.addChild(customIcon)
        customIcon.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(0.4, duration: 0.2),
                    SKAction.scaleTo(0.32, duration: 0.02)
                ])
            ])
        ]))
        
        moneyLabel = SKLabelNode(fontNamed: FontName_Money)
        moneyLabel.name = "moneyLabel"
        moneyLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        moneyLabel.fontColor = Color_Gold
        if (money > 0) {
            moneyLabel.text = String(money!)
        }
        moneyLabel.position = CGPointMake(40, 40)
        moneyLabel.alpha = 0
        moneyLabel.setScale(0.05)
        customIcon.addChild(moneyLabel)
        moneyLabel.runAction(SKAction.sequence([
            SKAction.waitForDuration(1.0),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(1.2, duration: 0.2),
                    SKAction.scaleTo(1.0, duration: 0.02)
                ])
            ]),
            SKAction.waitForDuration(0.3),
            SKAction.repeatActionForever(SKAction.sequence([
                SKAction.rotateByAngle(CGFloat(M_PI/20.0), duration: 0.2),
                SKAction.rotateByAngle(CGFloat(-M_PI/10.0), duration: 0.4),
                SKAction.rotateByAngle(CGFloat(M_PI/20.0), duration: 0.2)
            ]))
        ]))
        
        instructionBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            2,
            position: CGPointMake(self.frame.size.width, self.frame.size.height)
        )
        instructionBubble.name = "instructionBubble"
        instructionBubble.physicsBody!.dynamic = false
        instructionBubble.userInteractionEnabled = false
        self.addChild(instructionBubble)
        
        instructionLabel.position = CGPointMake(self.frame.size.width - 16, self.frame.size.height - 28)
        instructionLabel.name = "instructionLabel"
        instructionLabel.fontSize = 30
        instructionLabel.text = "?"
        self.addChild(instructionLabel)
        Utility.buttonLabelZoomIn(instructionLabel)

        infoBubble = BubbleFactory.spawnColorBubbleWithRandomColor(2, position: CGPointMake(0, self.frame.size.height))
        infoBubble.name = "infoBubble"
        infoBubble.physicsBody!.dynamic = false
        infoBubble.userInteractionEnabled = false
        self.addChild(infoBubble)
        
        infoLabel.position = CGPointMake(16, self.frame.size.height - 27)
        infoLabel.name = "infoLabel"
        infoLabel.fontSize = 30
        infoLabel.text = "i"
        self.addChild(infoLabel)
        Utility.buttonLabelZoomIn(infoLabel)
        
        ViewController.playMenuBgMusic()
        bgMusicPlayer.play()
    }
    
    override func update(currentTime: NSTimeInterval) {
        // Update StartLabel size to have a implusing effect
        if (startLabel.text == "Play") {
            startLabel.setScale(1 + sin(CGFloat(currentTime * startLabelSizeChangeSpeed)) * startLabelSizeChangeRange)
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil) {
                if ((node!.name == "startLabel" || node!.name == "startBubble") && !startLabel.hidden) {
                    if (startLabel.text == "Play") {
                        // Display select mode screen
                        displaySelectModeUIs()
                    }
                    else if (startLabel.text == "Back") {
                        // Go back to start screen
                        hideSelectModeUIs()
                    }
                }
                else if (node!.name == "gameCenterBubble" || node!.name == "gameCenterIcon") {
                    NSNotificationCenter.defaultCenter().postNotificationName("openGameCenter", object: nil)
                }
                else if (node!.name == "settingBubble" || node!.name == "settingIcon") {
                    if (userSettings.sfxEnabled) {
                        settingBubble.runAction(buttonSfxAction)
                    }
                    settingBubble.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.settingBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var settingScene:GameSettingScene = GameSettingScene(size: self.size)
                            self.view!.presentScene(settingScene, transition: transition)
                        })
                        ]))
                }
                else if (node!.name == "customBubble" || node!.name == "customIcon") {
                    if (userSettings.sfxEnabled) {
                        customBubble.runAction(buttonSfxAction)
                    }
                    customBubble.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.customBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var customizeScene:CustomizeScene = CustomizeScene(size: self.size)
                            self.view!.presentScene(customizeScene, transition: transition)
                        })
                        ]))
                }
                else if (node!.name == "instructionBubble" || node!.name == "instructionLabel") {
                    if (userSettings.sfxEnabled) {
                        instructionBubble.runAction(buttonSfxAction)
                    }
                    instructionBubble.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.instructionBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var instructionScene:InstructionScene = InstructionScene(size: self.size)
                            self.view!.presentScene(instructionScene, transition: transition)
                        })
                        ]))
                }
                else if (node!.name == "infoBubble" || node!.name == "infoLabel") {
                    if (userSettings.sfxEnabled) {
                        infoBubble.runAction(buttonSfxAction)
                    }
                    instructionBubble.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.infoBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var creditScene:CreditScene = CreditScene(size: self.size)
                            self.view!.presentScene(creditScene, transition: transition)
                        })
                        ]))
                }
                else if (node!.name == "timeModeBubble" || node!.name == "timeModeLabel") {
                    if (userSettings.sfxEnabled) {
                        timeModeBubble.runAction(buttonSfxAction)
                    }
                    self.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.timeModeBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var gameScene:GameTimeModeScene = GameTimeModeScene(size: self.size)
                            self.view!.presentScene(gameScene, transition: transition)
                        })
                        ]))
                }
                else if (node!.name == "endlessModeBubble" || node!.name == "endlessModeLabel") {
                    if (userSettings.sfxEnabled) {
                        endlessModeBubble.runAction(buttonSfxAction)
                    }
                    self.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.endlessModeBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var gameScene:GameEndlessModeScene = GameEndlessModeScene(size: self.size)
                            self.view!.presentScene(gameScene, transition: transition)
                        })
                        ]))
                }
                else if (node!.name == "challengeModeBubble" || node!.name == "challengeModeLabel") {
                    if (userSettings.sfxEnabled) {
                        challengeModeBubble.runAction(buttonSfxAction)
                    }
                    self.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.challengeModeBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            var gameScene:GameChallengeModeScene = GameChallengeModeScene(size: self.size)
                            self.view!.presentScene(gameScene, transition: transition)
                        })
                        ]))
                }
            }
        }
    }
    
    func displaySelectModeUIs() {
        titleLabel1.hidden = true
        titleLabel2.hidden = true
        gameCenterBubble.hidden = true
        gameCenterIcon.hidden = true
        settingBubble.hidden = true
        settingIcon.hidden = true
        customBubble.hidden = true
        customIcon.hidden = true
        
        startLabel.hidden = true
        startBubble.runAction(SKAction.scaleTo(0.7, duration: 0.2))
        
        self.runAction(SKAction.sequence([
            SKAction.runBlock({
                self.timeModeBubble = BubbleFactory.spawnColorBubbleWithColor(BubbleColor.Salmon, level: 1, position: self.startBubble.position)
                self.timeModeBubble.name = "timeModeBubble"
                self.timeModeBubble.userInteractionEnabled = false
                self.timeModeBubble.physicsBody!.dynamic = false
                self.addChild(self.timeModeBubble)
                self.timeModeBubble.runAction(SKAction.sequence([
                    SKAction.moveBy(CGVectorMake(0, 100), duration: 0.2),
                    SKAction.runBlock({
                        self.timeModeLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
                        self.timeModeLabel.position = CGPointMake(self.timeModeBubble.position.x, self.timeModeBubble.position.y - 8)
                        self.timeModeLabel.fontSize = 22
                        self.timeModeLabel.text = "Classic"
                        self.timeModeLabel.name = "timeModeLabel"
                        self.timeModeLabel.alpha = 0
                        self.timeModeLabel.setScale(0.05)
                        self.addChild(self.timeModeLabel)
                        Utility.buttonLabelZoomIn(self.timeModeLabel)
                    }),
                    SKAction.sequence([
                        SKAction.scaleTo(1.1, duration: 0.08),
                        SKAction.scaleTo(1.0, duration: 0.2)
                    ])
                ]))
                
                self.endlessModeBubble = BubbleFactory.spawnColorBubbleWithColor(BubbleColor.Flora, level: 1, position: self.startBubble.position)
                self.endlessModeBubble.name = "endlessModeBubble"
                self.endlessModeBubble.userInteractionEnabled = false
                self.endlessModeBubble.physicsBody!.dynamic = false
                self.addChild(self.endlessModeBubble)
                self.endlessModeBubble.runAction(SKAction.sequence([
                    SKAction.moveBy(CGVectorMake(-85, -53), duration: 0.2),
                    SKAction.runBlock({
                        self.endlessModeLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
                        self.endlessModeLabel.position = CGPointMake(self.endlessModeBubble.position.x, self.endlessModeBubble.position.y - 8)
                        self.endlessModeLabel.fontSize = 21
                        self.endlessModeLabel.text = "Endless"
                        self.endlessModeLabel.name = "endlessModeLabel"
                        self.endlessModeLabel.alpha = 0
                        self.endlessModeLabel.setScale(0.05)
                        self.addChild(self.endlessModeLabel)
                        Utility.buttonLabelZoomIn(self.endlessModeLabel)
                    }),
                    SKAction.sequence([
                        SKAction.scaleTo(1.1, duration: 0.08),
                        SKAction.scaleTo(1.0, duration: 0.2)
                    ])
                ]))
                
                self.challengeModeBubble = BubbleFactory.spawnColorBubbleWithColor(BubbleColor.Cantaloupe, level: 1, position: self.startBubble.position)
                self.challengeModeBubble.name = "challengeModeBubble"
                self.challengeModeBubble.userInteractionEnabled = false
                self.challengeModeBubble.physicsBody!.dynamic = false
                self.addChild(self.challengeModeBubble)
                self.challengeModeBubble.runAction(SKAction.sequence([
                    SKAction.moveBy(CGVectorMake(85, -53), duration: 0.2),
                    SKAction.runBlock({
                        self.challengeModeLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
                        self.challengeModeLabel.position = CGPointMake(self.challengeModeBubble.position.x, self.challengeModeBubble.position.y - 8)
                        self.challengeModeLabel.fontSize = 18
                        self.challengeModeLabel.text = "Challenge"
                        self.challengeModeLabel.name = "challengeModeLabel"
                        self.challengeModeLabel.alpha = 0
                        self.challengeModeLabel.setScale(0.05)
                        self.addChild(self.challengeModeLabel)
                        Utility.buttonLabelZoomIn(self.challengeModeLabel)
                    }),
                    SKAction.sequence([
                        SKAction.scaleTo(1.1, duration: 0.08),
                        SKAction.scaleTo(1.0, duration: 0.2)
                    ])
                ]))
            }),
            SKAction.waitForDuration(0.2),
            SKAction.runBlock({
                self.startLabel.text = "Back"
                self.startLabel.fontSize = 17
                self.startLabel.position = CGPointMake(self.startBubble.position.x, self.startBubble.position.y - 6)
                self.startLabel.alpha = 0
                self.startLabel.setScale(0.05)
                self.startLabel.hidden = false
                Utility.buttonLabelZoomIn(self.startLabel)
            })
        ]))
    }
    
    func hideSelectModeUIs() {
        startLabel.hidden = true
        titleLabel1.hidden = true
        
        startBubble.runAction(SKAction.sequence([
            SKAction.scaleTo(1.0, duration: 0.2),
            SKAction.scaleTo(1.1, duration: 0.08),
            SKAction.scaleTo(1.0, duration: 0.2)
        ]))
        
        self.runAction(SKAction.sequence([
            SKAction.runBlock({
                self.timeModeLabel.hidden = true
                self.timeModeBubble.runAction(SKAction.sequence([
                    SKAction.moveBy(CGVectorMake(0, -100), duration: 0.2),
                    SKAction.removeFromParent()
                ]))
                
                self.endlessModeLabel.hidden = true
                self.endlessModeBubble.runAction(SKAction.sequence([
                    SKAction.moveBy(CGVectorMake(85, 53), duration: 0.2),
                    SKAction.removeFromParent()
                ]))
                
                self.challengeModeLabel.hidden = true
                self.challengeModeBubble.runAction(SKAction.sequence([
                    SKAction.moveBy(CGVectorMake(-85, 53), duration: 0.2),
                    SKAction.removeFromParent()
                ]))
            }),
            SKAction.waitForDuration(0.2),
            SKAction.runBlock({
                self.startLabel.text = "Play"
                self.startLabel.fontSize = 35
                self.startLabel.position = CGPointMake(self.startBubble.position.x, self.startBubble.position.y - 12)
                self.startLabel.alpha = 0
                self.startLabel.setScale(0.05)
                self.startLabel.hidden = false
                self.startLabel.runAction(SKAction.fadeInWithDuration(0.2))
                
                self.titleLabel1.hidden = false
                self.titleLabel2.hidden = false
                
                self.gameCenterBubble.hidden = false
                self.gameCenterIcon.hidden = false
                self.settingBubble.hidden = false
                self.settingIcon.hidden = false
                self.customBubble.hidden = false
                self.customIcon.hidden = false
            })
        ]))
    }
    
    class func loadSceneAssets() {
        // UI sfx
        buttonSfxAction = SKAction.playSoundFileNamed("button.wav", waitForCompletion: false)
        moneySfxAction = SKAction.playSoundFileNamed("coin.wav", waitForCompletion: false)
        digitSfxAction = SKAction.playSoundFileNamed("digit.wav", waitForCompletion: false)
    }
}
