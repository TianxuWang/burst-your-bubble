//
//  CreditScene.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/19/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class CreditScene: SKScene {
   
    var backBubble:ColorBubble!
    var backIcon:SKSpriteNode!
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        
        var titleLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        titleLabel.fontColor = Color_Gray
        titleLabel.fontSize = 40
        titleLabel.text = "Credits"
        titleLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 180)
        titleLabel.zPosition = 1
        self.addChild(titleLabel)
        
        var subTitleLabel1 = SKLabelNode(fontNamed: FontName_DefaultLight)
        subTitleLabel1.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        subTitleLabel1.fontColor = Color_Gray
        subTitleLabel1.fontSize = 13
        subTitleLabel1.text = "Code & Design"
        subTitleLabel1.position = CGPointMake(self.frame.size.width / 2 - 100, self.frame.size.height / 2 + 105)
        subTitleLabel1.zPosition = 1
        self.addChild(subTitleLabel1)
        
        var contentLabel1 = SKLabelNode(fontNamed: FontName_DefaultBold)
        contentLabel1.fontColor = Color_Gray
        contentLabel1.fontSize = 20
        contentLabel1.text = "Tianxu Wang"
        contentLabel1.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 80)
        contentLabel1.zPosition = 1
        self.addChild(contentLabel1)
        
        var subTitleLabel2 = SKLabelNode(fontNamed: FontName_DefaultLight)
        subTitleLabel2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        subTitleLabel2.fontColor = Color_Gray
        subTitleLabel2.fontSize = 13
        subTitleLabel2.text = "Music & Sound"
        subTitleLabel2.position = CGPointMake(self.frame.size.width / 2 - 100, self.frame.size.height / 2 + 25)
        subTitleLabel2.zPosition = 1
        self.addChild(subTitleLabel2)
        
        var contentLabel2 = SKLabelNode(fontNamed: FontName_DefaultBold)
        contentLabel2.fontColor = Color_Gray
        contentLabel2.fontSize = 20
        contentLabel2.text = "stereoesque"
        contentLabel2.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)
        contentLabel2.zPosition = 1
        self.addChild(contentLabel2)
        
        var subTitleLabel3 = SKLabelNode(fontNamed: FontName_DefaultLight)
        subTitleLabel3.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        subTitleLabel3.fontColor = Color_Gray
        subTitleLabel3.fontSize = 13
        subTitleLabel3.text = "Quality Assurance"
        subTitleLabel3.position = CGPointMake(self.frame.size.width / 2 - 100, self.frame.size.height / 2 - 55)
        subTitleLabel3.zPosition = 1
        self.addChild(subTitleLabel3)
        
        var contentLabel3 = SKLabelNode(fontNamed: FontName_DefaultBold)
        contentLabel3.fontColor = Color_Gray
        contentLabel3.fontSize = 20
        contentLabel3.text = "Rui Wang"
        contentLabel3.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 80)
        contentLabel3.zPosition = 1
        self.addChild(contentLabel3)
        
        // Back button
        backBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            2,
            position: CGPointMake(0, self.frame.size.height)
        )
        backBubble.userInteractionEnabled = false
        backBubble.name = "backBubble"
        self.addChild(backBubble)
        
        backIcon = SKSpriteNode(imageNamed: "back.png")
        backIcon.name = "backIcon"
        backIcon.position = CGPointMake(18, self.frame.size.height - 18)
        backIcon.setScale(0.05)
        backIcon.alpha = 0
        self.addChild(backIcon)
        backIcon.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(0.28, duration: 0.2),
                    SKAction.scaleTo(0.23, duration: 0.02)
                ])
            ])
        ]))
        
        var emitter_left = BubbleEmitter(node: self, name: "leftEmitter", level: 6, pos: CGPointMake(0, 0), directionX: true, directionY: true, interval: 0.02, lastTime: 0.4, rangeTime: 0.2)
        var emitter_mid = BubbleEmitter(node: self, name: "midEmitter", level: 6, pos: CGPointMake(self.frame.size.width / 2, 0), directionX: nil, directionY: true, interval: 0.02, lastTime: 0.4, rangeTime: 0.2)
        var emitter_right = BubbleEmitter(node: self, name: "rightEmitter", level: 6, pos: CGPointMake(self.frame.size.width, 0), directionX: false, directionY: true, interval: 0.02, lastTime: 0.4, rangeTime: 0.2)
        
        emitter_left.emit()
        emitter_mid.emit()
        emitter_right.emit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil ) {
                if (node!.name == "backBubble" || node!.name == "backIcon") {
                    if (userSettings.sfxEnabled) {
                        backBubble.runAction(buttonSfxAction)
                    }
                    backBubble.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.backBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var menuScene:GameMenuScene = GameMenuScene(size: self.size)
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            self.view!.presentScene(menuScene, transition: transition)
                        })
                        ]))
                }
            }
        }
    }
}
