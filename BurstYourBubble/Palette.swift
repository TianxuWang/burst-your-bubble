//
//  Palette.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/11/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

let PaletteNum = 6

class Palette {
    
    var id:Int!
    var name:String!
    var price:Int!
    var colorNum:Int!
    var displayImg:SKTexture!
    var colors:[SKColor]!
    
    var bubbleAnimFrames:[[SKTexture]] = [[SKTexture]]()
    var boundaryTextures:[SKTexture] = [SKTexture]()
    
    init(id:Int, name:String, price:Int, colors:[SKColor]) {
        self.id = id
        self.name = name
        self.price = price
        self.colorNum = colors.count
        self.colors = colors
        
        self.displayImg = SKTexture(imageNamed: "palette_display_\(id)")
        
        for var i = 0; i < colorNum; i++ {
            var colorBubbleFileName = "\(BubbleColor.ColorPool[i].toString())_\(id)"
            bubbleAnimFrames.append(Utility.loadFramesWithName(colorBubbleFileName, numberOfFrames: ColorBubbleAnimationFrameNum))
            
            var boundaryFileName = "\(BubbleColor.ColorPool[i].toString())_boundary_\(id)"
            boundaryTextures.append(SKTexture(imageNamed: boundaryFileName))
        }
        
        var paintBubbleFileName = "Paint_\(id)"
        bubbleAnimFrames.append(Utility.loadFramesWithName(paintBubbleFileName, numberOfFrames: colorNum))
    }
    
    class func update() {
        curPalette = palettes[userSettings.selectedPaletteId]
        bubbleAnimationFrames = curPalette.bubbleAnimFrames
        colorBoundaryTextures = curPalette.boundaryTextures
    }
}

let Colors_Palette_0:[SKColor] =
[
    SKColor(red: 254/255, green: 67/255, blue: 101/255, alpha: 1.0),
    SKColor(red: 252/255, green: 157/255, blue: 154/255, alpha: 1.0),
    SKColor(red: 249/255, green: 205/255, blue: 173/255, alpha: 1.0),
    SKColor(red: 200/255, green: 200/255, blue: 169/255, alpha: 1.0),
    SKColor(red: 131/255, green: 175/255, blue: 155/255, alpha: 1.0)
]

let Colors_Palette_1:[SKColor] =
[
    SKColor(red: 245/255, green: 105/255, blue: 145/255, alpha: 1.0),
    SKColor(red: 1, green: 159/255, blue: 128/255, alpha: 1.0),
    SKColor(red: 1, green: 196/255, blue: 140/255, alpha: 1.0),
    SKColor(red: 239/255, green: 250/255, blue: 180/255, alpha: 1.0),
    SKColor(red: 209/255, green: 242/255, blue: 165/255, alpha: 1.0)
]

let Colors_Palette_2:[SKColor] =
[
    SKColor(red: 1, green: 0, blue: 60/255, alpha: 1.0),
    SKColor(red: 1, green: 138/255, blue: 0, alpha: 1.0),
    SKColor(red: 250/255, green: 190/255, blue: 40/255, alpha: 1.0),
    SKColor(red: 204/255, green: 191/255, blue: 24/255, alpha: 1.0),
    SKColor(red: 136/255, green: 193/255, blue: 0, alpha: 1.0),
    SKColor(red: 0, green: 193/255, blue: 118/255, alpha: 1.0),
    SKColor(red: 0, green: 160/255, blue: 176/255, alpha: 1.0),
    SKColor(red: 174/255, green: 95/255, blue: 131/255, alpha: 1.0)
]

let Colors_Palette_3:[SKColor] =
[
    SKColor(red: 1, green: 101/255, blue: 98/255, alpha: 1.0),
    SKColor(red: 1, green: 205/255, blue: 93/255, alpha: 1.0),
    SKColor(red: 1, green: 1, blue: 88/255, alpha: 1.0),
    SKColor(red: 203/255, green: 1, blue: 90/255, alpha: 1.0),
    SKColor(red: 94/255, green: 1, blue: 92/255, alpha: 1.0),
    SKColor(red: 94/255, green: 1, blue: 203/255, alpha: 1.0),
    SKColor(red: 93/255, green: 1, blue: 1, alpha: 1.0),
    SKColor(red: 97/255, green: 203/255, blue: 1, alpha: 1.0),
    SKColor(red: 101/255, green: 96/255, blue: 1, alpha: 1.0),
    SKColor(red: 205/255, green: 95/255, blue: 1, alpha: 1.0),
    SKColor(red: 1, green: 107/255, blue: 208/255, alpha: 1.0)
]

let Colors_Palette_4:[SKColor] =
[
    SKColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0),
    SKColor(red: 217/255, green: 213/255, blue: 217/255, alpha: 1.0),
    SKColor(red: 212/255, green: 195/255, blue: 206/255, alpha: 1.0),
    SKColor(red: 227/255, green: 202/255, blue: 208/255, alpha: 1.0),
    SKColor(red: 232/255, green: 228/255, blue: 216/255, alpha: 1.0),
    SKColor(red: 236/255, green: 240/255, blue: 228/255, alpha: 1.0)
]

let Colors_Palette_5:[SKColor] =
[
    SKColor(red: 189/255, green: 131/255, blue: 199/255, alpha: 1.0),
    SKColor(red: 130/255, green: 85/255, blue: 145/255, alpha: 1.0),
    SKColor(red: 66/255, green: 31/255, blue: 60/255, alpha: 1.0),
    SKColor(red: 105/255, green: 28/255, blue: 72/255, alpha: 1.0),
    SKColor(red: 150/255, green: 55/255, blue: 79/255, alpha: 1.0),
    SKColor(red: 186/255, green: 78/255, blue: 114/255, alpha: 1.0)
]
