//
//  GamePreloadScene.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/31/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class GamePreloadScene: SKScene {
    
    let tips:[String] =
    [
        "Look for the pulsing boundary if you aren't sure about the current color",
        "Bomb bubble will burst spontaneously when its speed is very slow",
        "Try using two fingers!",
        "Smaller bubble in the same color gives you more points",
        "Smaller paint and bomb bubbles have larger effective area when bursted",
        "Try different combination of palettes and sound packs!",
        "Palette with more colors is harder to play with but faster to get points",
        "Tapping on bubbles with the same color will blow up a random bubble"
    ]
    
    var progressBar:ProgressBar!
    var tipLabel:SKLabelNode!
    
    override init(size: CGSize) {
        super.init(size: size)
        self.backgroundColor = Color_Black
        
        initProgressBar()
        initTip()
        
        self.loadAssetsWithCompletionHandler({ () -> Void in
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(1.0),
                SKAction.runBlock({
                    NSNotificationCenter.defaultCenter().postNotificationName("loadAdBanner", object: nil)
                    var scene:SKScene = isFirstLaunch ? InstructionScene(size: self.size) : GameMenuScene(size: self.size)
                    scene.scaleMode = SKSceneScaleMode.AspectFill
                    var transition:SKTransition = SKTransition.fadeWithColor(scene.backgroundColor, duration: 1.0)
                    self.view!.presentScene(scene, transition: transition)
                })
            ]))}
        )
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initProgressBar() {
        progressBar = ProgressBar(
            imageName: "palette_display_\(userSettings.selectedPaletteId)",
            position: CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)
        )
        
        self.addChild(progressBar)
    }
    
    func initTip() {
        tipLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        tipLabel.fontColor = Color_White
        tipLabel.fontSize = 10
        tipLabel.text = tips[Int(arc4random_uniform(UInt32(tips.count)))]
        tipLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 40)
        addChild(tipLabel)
    }
    
    func loadAssetsWithCompletionHandler(completionHandler: () -> Void) {
        let queue = dispatch_get_main_queue()
        
        let backgroundQueue = dispatch_get_global_queue(CLong(DISPATCH_QUEUE_PRIORITY_HIGH), 0)
        dispatch_async(backgroundQueue) {
            // Preload musics
            bgMusicPlayer = AVQueuePlayer()
            bgMusicPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.None
            // If other app is playing sound, turn off the music player
            userSettings.bgMusicEnabled = userSettings.bgMusicEnabled && !AVAudioSession.sharedInstance().otherAudioPlaying
            bgMusicPlayer.volume = userSettings.bgMusicEnabled ? 1.0 : 0.0
            
            menuBgMusic = AVPlayerItem(URL: NSBundle.mainBundle().URLForResource("menu_0", withExtension: "m4a")!)
            
            soundThemes.append(SoundTheme(id: 0, name: "Falling for 'roy", price: 0, musicFormat: "m4a"))
            soundThemes.append(SoundTheme(id: 1, name: "Soothe Me", price: 2000, musicFormat: "mp3"))
            soundThemes.append(SoundTheme(id: 2, name: "The Old Woods", price: 2000, musicFormat: "m4a"))
            soundThemes.append(SoundTheme(id: 3, name: "Transubstantial", price: 2000, musicFormat: "m4a"))
            soundThemes.append(SoundTheme(id: 4, name: "Nichtendrepn", price: 2000, musicFormat: "m4a"))
            SoundTheme.update()
            
            dispatch_async(queue, {
                self.progressBar.setProgress(0.2)
            })
            
            GameMenuScene.loadSceneAssets()
            dispatch_async(queue, {
                self.progressBar.setProgress(0.3)
            })
            
            GameScene.loadSceneAssets()
            dispatch_async(queue, {
                self.progressBar.setProgress(0.4)
            })
            
            GameOverScene.loadSceneAssets()
            dispatch_async(queue, {
                self.progressBar.setProgress(0.5)
            })
            
            Bubble.loadAssets()
            dispatch_async(queue, {
                self.progressBar.setProgress(1.0)
            })
            
            dispatch_async(queue, completionHandler)
        }
    }
}
