//
//  BombBubble.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/16/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit
import AudioToolbox

class BombBubble: Bubble {
    
    var sprites_Normal:[SKTexture]!
    var sprites_Scale:[SKTexture]!
    var changeSizeAnimAction:SKAction!
    
    // Used for bonus time calculation in Endless mode
    var bonusTime:Int = 0
    var bonusTimeMultiplier:Int = 1
    
    override init(level: Int, position: CGPoint) {
        super.init(level: level, position: position)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func initializeSprites() {
        self.spriteArray = Bubble.getAnimationFramesForBombBubble()
        
        self.sprites_Normal = Array(spriteArray[0..<7])
        
        self.sprites_Scale = Array(spriteArray[7..<BombBubbleAnimationFrameNum])
        self.sprites_Scale = sprites_Scale + sprites_Scale.reverse()
        self.sprites_Scale.append(sprites_Normal[0])
        
        self.texture = self.spriteArray[0]
    }
    
    override func initializeAttributes() {
        self.size = self.texture!.size()
        super.initializeAttributes()
    }
    
    override func initializePhysics() {
        super.initializePhysics()
        
        self.physicsBody!.allowsRotation = true
        self.physicsBody!.collisionBitMask = ColorBubbleCategory | MovingBubbleCategory
    }
    
    override func initializeActions() {
        super.initializeActions()
        
        self.changeSizeAnimAction = SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.scaleBy(1.1, duration: 0.5),
                SKAction.scaleBy(0.9, duration: 0.5)
            ])
        )
        self.blowUpAnimAction = SKAction.sequence([
            SKAction.runBlock({
                self.runAction(SKAction.group([
                    SKAction.scaleTo(self.getScaleByLevel(self.level), duration: 0.7),
                    SKAction.animateWithTextures(self.sprites_Scale, timePerFrame: 0.07)
                    ]))
            }),
            SKAction.waitForDuration(0.7),
            SKAction.runBlock({
                var velocity = self.physicsBody!.velocity
                self.initializePhysics()
                self.physicsBody!.categoryBitMask = MovingBubbleCategory
                self.physicsBody!.contactTestBitMask = BombBubbleCategory
                // Slightly decrease the speed as the size has been increased.
                self.physicsBody!.velocity = CGVectorMake(velocity.dx * 0.8, velocity.dy * 0.8)
                
                var scene = self.scene! as! GameScene
                if (scene.gameStarted) {
                    self.userInteractionEnabled = true
                }
                
                self.runAction(self.changeSizeAnimAction, withKey: "changeSize")
            })
        ])
        
        self.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.fadeAlphaTo(0.8, duration: 0.05),
                SKAction.scaleTo(1 - 0.1 * CGFloat(level), duration: 0.2)
                ]),
            SKAction.runBlock({
                self.physicsBody!.categoryBitMask = MovingBubbleCategory
                self.physicsBody!.contactTestBitMask = BombBubbleCategory
                
                self.runAction(self.changeSizeAnimAction, withKey: "changeSize")
            })
        ]))
    }
    
    override func update() {
        super.update()
        
        if (self.physicsBody!.velocity.dx == 0 &&
            self.physicsBody!.velocity.dy == 0 &&
            self.isAlive && self.userInteractionEnabled) {
            
            if (userSettings.sfxEnabled) {
                self.runAction(bombDischargeSfxAction)
            }
            
            self.pop()
        }

    }
    
    override func pop() {
        self.removeActionForKey("levelDown")
        self.removeActionForKey("changeSize")
        self.zPosition = -1
        self.alpha = 1
        self.isAlive = false
        self.userInteractionEnabled = false
        
        var temp:SKPhysicsBody = self.physicsBody!
        temp.dynamic = false
        self.physicsBody = temp
        
        var scale:CGFloat = 1.5 + CGFloat(self.level) / 8
        
        if (userSettings.sfxEnabled) {
            if (bonusTimeMultiplier > 1) {
                self.runAction(bombDischargeSfxAction)
            }
            self.runAction(SKAction.sequence([
                bombArmSfxAction,
                SKAction.waitForDuration(0.5),
                bombPopSfxAction
            ]))
        }
        
        if (userSettings.vibrationEnabled) {
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(0.5),
                SKAction.runBlock({
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                })
            ]))
        }
        
        self.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.runBlock({
                    Utility.shake(self, amplitude: UInt32(7 - self.level / 2), duration: 0.5, frequency: 20)
                }),
                SKAction.sequence([
                    SKAction.fadeAlphaTo(0.5, duration: 0.25),
                    SKAction.fadeAlphaTo(1.0, duration: 0.25)
                ])
            ]),
            SKAction.runBlock({
                self.physicsBody!.categoryBitMask = 0x0
                self.physicsBody!.collisionBitMask = 0x0
            }),
            SKAction.group([
                SKAction.animateWithTextures(self.sprites_Normal, timePerFrame: 0.05),
                SKAction.scaleTo(scale, duration: 0.15)
            ]),
            SKAction.runBlock({
                self.physicsBody = SKPhysicsBody(circleOfRadius: self.frame.size.width/2 * 0.85)
                self.physicsBody!.categoryBitMask = BombBubbleCategory
                self.physicsBody!.contactTestBitMask = ColorBubbleCategory | MovingBubbleCategory
                self.physicsBody!.collisionBitMask = 0x0
            }),
            SKAction.fadeOutWithDuration(1.0)
            ]),
            completion: {
                if (self.scene! is GameEndlessModeScene && self.bonusTime > 0) {
                    var scene = self.scene! as! GameEndlessModeScene
                    scene.addClockIcon(self.bonusTime, bonusTimeMultiplier: self.bonusTimeMultiplier, position: self.position)
                }
                self.removeFromParent()
            }
        )
        
        if (self.scene! is GameScene && gameCenterEnabled) {
            // Check Total Bomb Bubble achievement
            userSettings.achievementsTrack[2]++
            switch(userSettings.achievementsTrack[2]) {
            case 10..<100:
                GameCenterHelper.reportAchievement(AchievementID_TBB10)
            case 100..<500:
                GameCenterHelper.reportAchievement(AchievementID_TBB100)
            case 500..<1000:
                GameCenterHelper.reportAchievement(AchievementID_TBB500)
            default:
                if (userSettings.achievementsTrack[2] >= 1000) {
                    GameCenterHelper.reportAchievement(AchievementID_TBB1000)
                }
            }
        }
        
        super.pop()
    }
    
    override func decreaseOneLevel() {
        if (self.level > 0) {
            // Remove interactions
            self.removeActionForKey("changeSize")
            self.setScale(1 - 0.1 * CGFloat(level))
            self.userInteractionEnabled = false
            self.physicsBody!.categoryBitMask = 0x0
            self.physicsBody!.contactTestBitMask = 0x0
            
            self.level--           
            self.runAction(blowUpAnimAction, withKey: "levelDown")
        }
    }
}
