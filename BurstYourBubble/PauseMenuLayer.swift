//
//  PauseMenuView.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/17/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class PauseMenuLayer: SKSpriteNode {
    
    var resumeBubble:ColorBubble!
    var restartBubble:ColorBubble!
    var resumeLabel:SKLabelNode!
    var restartLabel:SKLabelNode!
    var menuBubble:ColorBubble!
    var menuLabel:SKLabelNode!
    
    override init(texture: SKTexture?, color: UIColor?, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    init(size: CGSize) {
        super.init(texture: nil, color: SKColor(red: 0, green: 0, blue: 0, alpha: 0.6), size: size)
        
        self.name = "pauseMenuLayer"
        self.userInteractionEnabled = true
        
        menuBubble = BubbleFactory.spawnColorBubbleWithRandomColor(1, position: CGPointMake(0, 120))
        menuBubble.name = "menuBubble"
        menuBubble.physicsBody!.dynamic = false
        menuBubble.userInteractionEnabled = false
        self.addChild(menuBubble)
        
        menuLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        menuLabel.name = "menuLabel"
        menuLabel.fontSize = 22
        menuLabel.text = "Menu"
        menuLabel.position = CGPointMake(0, 113)
        self.addChild(menuLabel)
        menuLabel.userInteractionEnabled = false
        Utility.buttonLabelZoomIn(menuLabel)
        
        restartBubble = BubbleFactory.spawnColorBubbleWithRandomColor(1, position: CGPointMake(0, 0))
        restartBubble.name = "restartBubble"
        restartBubble.physicsBody!.dynamic = false
        restartBubble.userInteractionEnabled = false
        self.addChild(restartBubble)
        
        restartLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        restartLabel.name = "restartLabel"
        restartLabel.fontSize = 21
        restartLabel.text = "Restart"
        restartLabel.position = CGPointMake(0, -7)
        self.addChild(restartLabel)
        restartLabel.userInteractionEnabled = false
        Utility.buttonLabelZoomIn(restartLabel)
        
        resumeBubble = BubbleFactory.spawnColorBubbleWithRandomColor(1, position: CGPointMake(0, -120))
        resumeBubble.name = "resumeBubble"
        resumeBubble.physicsBody!.dynamic = false
        resumeBubble.userInteractionEnabled = false
        self.addChild(resumeBubble)
        
        resumeLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        resumeLabel.name = "resumeLabel"
        resumeLabel.fontSize = 21
        resumeLabel.text = "Resume"
        resumeLabel.position = CGPointMake(0, -127)
        self.addChild(resumeLabel)
        resumeLabel.userInteractionEnabled = false
        Utility.buttonLabelZoomIn(resumeLabel)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil) {
                if (node!.name == "resumeBubble" || node!.name == "resumeLabel") {
                    var gameScene:GameScene = self.scene! as! GameScene
                    gameScene.gameUnpause()
                }
                else if (node!.name == "restartBubble" || node!.name == "restartLabel") {
                    if (userSettings.sfxEnabled) {
                        restartBubble.runAction(buttonSfxAction)
                    }
                    // save achievement progress
                    NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
                    
                    Utility.shake(restartBubble, amplitude: 4, duration: 0.5, frequency: 20)
                    var gameScene:GameScene = self.scene! as! GameScene
                    gameScene.gameRestart()
                }
                else if (node!.name == "menuBubble" || node!.name == "menuLabel") {
                    if (userSettings.sfxEnabled) {
                        restartBubble.runAction(buttonSfxAction)
                    }
                    // save achievement progress
                    NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
                    
                    Utility.shake(menuBubble, amplitude: 4, duration: 0.5, frequency: 20)
                    self.runAction(SKAction.sequence([
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var menuScene:GameMenuScene = GameMenuScene(size: self.size)
                            var transition:SKTransition = SKTransition.fadeWithColor(self.scene!.backgroundColor, duration: 0.5)
                            self.scene!.view!.presentScene(menuScene, transition: transition)
                        })
                    ]))
                }
            }
        }
    }
}
