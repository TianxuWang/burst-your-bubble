//
//  Bubble.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/27/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit


//let MaxExternalImpluseTolerance:CGFloat = 1500000
let MaxLevel:Int = 9
let ColorBubbleAnimationFrameNum:Int = 9
let BombBubbleAnimationFrameNum:Int = 12
let BubblePopScaleSfxNum:Int = 27
let SpawnImpulse:Int = 15

let ColorBubbleCategory:UInt32 = 0x1 << 0
let MovingBubbleCategory:UInt32 = 0x1 << 1
let PaintBubbleCategory:UInt32 = 0x1 << 2
let BombBubbleCategory:UInt32 = 0x1 << 3

var bubbleAnimationFrames = [[SKTexture]]()
var bombAnimationFrames = [SKTexture]()

var bubblePopSfxAction = SKAction()
var paintSfxAction = SKAction()
var bombDischargeSfxAction = SKAction()
var bombArmSfxAction = SKAction()
var bombPopSfxAction = SKAction()
var bubbleBonusSfxActions = [SKAction]()

var kLoadBubbleAssetsOnceToken: dispatch_once_t = 0

class Bubble: SKSpriteNode {
    
    var spriteArray:[SKTexture]!
    var level:Int = 0
    var isAlive:Bool = true
    var blowUpAnimAction:SKAction!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(texture: SKTexture?, color: UIColor?, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    init(level:Int, position:CGPoint) {
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSizeZero)
        self.level = level
        self.position = position
        
        initializeSprites()
        initializeAttributes()
        initializePhysics()
        initializeActions()
    }

    func initializeSprites() {
        
    }
    
    func initializeAttributes() {
        // MUST set actual size before initializing physics body since size will take effect to mass
        self.setScale(1 - 0.1 * CGFloat(level))
        
        // Enable touch event. Easy to miss!
        self.userInteractionEnabled = true
        self.name = "bubble"
    }
    
    func initializePhysics() {
        // Use 'self.frame.size.width' will cause paint and bomb bubble to have incorrect physics body.
        self.physicsBody = SKPhysicsBody(circleOfRadius: self.size.width / 2 - 1)
        self.physicsBody!.dynamic = true
        self.physicsBody!.friction = 0.1
        self.physicsBody!.restitution = 0.8
        self.physicsBody!.linearDamping = 0.1
        self.physicsBody!.angularDamping = 0.1
        self.physicsBody!.allowsRotation = false
    }
    
    func initializeActions() {
        // Some sugar that make it more bubble-like:
        // bubble starts with very small size with alpha = 0 then will animate to actual size determined by level
        self.setScale(0.05)
        self.alpha = 0
    }
    
    // Use touchesBegan instead of touchesEnd to mimic bubble popping.
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.pop()
    }
    
    func pop() {
        // Remove this bubble from bubble pool.
        if (self.scene! is GameScene) {
            var scene:GameScene = self.scene! as! GameScene
            var index:Int? = find(scene.bubblePool, self)
            if (index != nil) {
                scene.bubblePool.removeAtIndex(index!)
            }
        }
    }
    
    func applyRandomImpulse() {
        var flipX:Bool = CGFloat(arc4random_uniform(2)) > 0
        var flipY:Bool = CGFloat(arc4random_uniform(2)) > 0
        
        self.applyRandomImpulseOnDirection(flipX, directionY: flipY)
    }
    
    func applyRandomImpulseOnDirection(directionX: Bool, directionY: Bool) {
        var impluseX:CGFloat = CGFloat(arc4random_uniform(UInt32(SpawnImpulse)))
        var impluseY:CGFloat = sqrt(CGFloat(SpawnImpulse * SpawnImpulse) - impluseX * impluseX)
        
        impluseX = directionX ? impluseX : -impluseX
        impluseY = directionY ? impluseY : -impluseY
        
        self.physicsBody!.applyImpulse(CGVectorMake(impluseX, impluseY))
    }
    
    func applyImpulseOnX(direction:Bool) {
        var directionY:Bool = CGFloat(arc4random_uniform(2)) > 0
        self.applyRandomImpulseOnDirection(direction, directionY: directionY)
    }
    
    func applyImpulseOnY(direction:Bool) {
        var directionX:Bool = CGFloat(arc4random_uniform(2)) > 0
        self.applyRandomImpulseOnDirection(directionX, directionY: direction)
    }
    
    func decreaseOneLevel() {
        
    }
    
    func isAbleToIncreaseSize() -> Bool {
        return self.isAlive && self.level > 0
    }
    
    func isAbleToPlaySfx() -> Bool {
        return self.scene != nil && (self.scene! is GameScene || self.scene! is GameOverScene || self.scene! is InstructionScene)
    }
    
    func update() {
        // burst the bubble if it hasn't been outside of the scene somehow
        if (self.isAlive &&
            (self.position.x < BoundaryThickness ||
            self.position.x > self.scene!.frame.width - BoundaryThickness ||
            self.position.y < BoundaryThickness ||
            self.position.y > self.scene!.frame.height - BoundaryThickness)) {
            self.pop()
        }
    }
    
    func getScaleByLevel(level: Int) -> CGFloat {
        return (1 - 0.1 * CGFloat(level))
    }
    
    // Used to preload assets of bubble
    class func loadAssets() {
        dispatch_once(&kLoadBubbleAssetsOnceToken) {
            // Preload sprites
            var palette_0 = Palette(id: 0, name: "Sugar", price: 0, colors: Colors_Palette_0)
            var palette_1 = Palette(id: 1, name: "Mellon Ball", price: 1000, colors: Colors_Palette_1)
            var palette_2 = Palette(id: 2, name: "Sweet Lolly", price: 1000, colors: Colors_Palette_2)
            var palette_3 = Palette(id: 3, name: "Crayons", price: 1000, colors: Colors_Palette_3)
            var palette_4 = Palette(id: 4, name: "Marshmallow", price: 1000, colors: Colors_Palette_4)
            var palette_5 = Palette(id: 5, name: "Flora", price: 1000, colors: Colors_Palette_5)
            
            palettes.append(palette_0)
            palettes.append(palette_1)
            palettes.append(palette_2)
            palettes.append(palette_3)
            palettes.append(palette_4)
            palettes.append(palette_5)
            Palette.update()
            
            bombAnimationFrames = Utility.loadFramesWithName("Bomb", numberOfFrames: BombBubbleAnimationFrameNum)
            
            // Preload sfx as actions
            bubblePopSfxAction = SKAction.playSoundFileNamed("colorbubble_pop.wav", waitForCompletion: false)
            bombDischargeSfxAction = SKAction.playSoundFileNamed("bombbubble_discharge.wav", waitForCompletion: false)
            bombArmSfxAction = SKAction.playSoundFileNamed("bombbubble_arming.wav", waitForCompletion: false)
            bombPopSfxAction = SKAction.playSoundFileNamed("bombbubble_short.wav", waitForCompletion: false)
            paintSfxAction = SKAction.playSoundFileNamed("paintbubble_pop.wav", waitForCompletion: false)
        }
    }
    
    class func getAnimationFramesForColorBubble(color: BubbleColor) -> [SKTexture] {
        return bubbleAnimationFrames[color.rawValue]
    }
    
    class func getAnimationFramesForPaintBubble() -> [SKTexture] {
        return bubbleAnimationFrames.last!
    }
    
    class func getAnimationFramesForBombBubble() -> [SKTexture] {
        return bombAnimationFrames
    }
}


