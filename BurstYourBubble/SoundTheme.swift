//
//  SoundTheme.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/9/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit
import AVFoundation

let SoundThemeNum = 5

class SoundTheme {
    
    var id:Int!
    var name:String!
    var price:Int!
    var scaleSfxActions:[SKAction]!
    var bgMusic:AVPlayerItem!
    
    init(id: Int, name: String, price: Int, musicFormat: String) {
        self.id = id
        self.name = name
        self.price = price
        var musicFileName:String = "track_\(id)"
        var sfxFileName:String = "scale_\(id)"
        
        bgMusic = AVPlayerItem(URL: NSBundle.mainBundle().URLForResource(musicFileName, withExtension: musicFormat)!)
        scaleSfxActions = Utility.loadSoundActionsWithName(sfxFileName, numberOfFiles: BubblePopScaleSfxNum)
    }
    
    class func update() {
        curSoundTheme = soundThemes[userSettings.selectedMusicThemeId]
        bubbleBonusSfxActions = curSoundTheme.scaleSfxActions
    }
}
