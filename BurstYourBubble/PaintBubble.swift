//
//  DyeBubble.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/15/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class PaintBubble: Bubble {
    
    var paintColor:BubbleColor?
    var sprites_Idle:[SKTexture]!
    var sprites_Pop:[SKTexture]!
    var sprites_Scale:[SKTexture]!
    var changeColorAnimAction:SKAction!
    var changeSizeAnimAction:SKAction!
    
    override init(level: Int, position: CGPoint) {
        super.init(level: level, position: position)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func initializeSprites() {
        self.spriteArray = Bubble.getAnimationFramesForPaintBubble()
        self.sprites_Idle = spriteArray
        self.sprites_Scale = Array(Bubble.getAnimationFramesForColorBubble(BubbleColor.randomColor())[4..<ColorBubbleAnimationFrameNum])
        self.sprites_Scale = sprites_Scale + sprites_Scale.reverse()
        self.texture = self.spriteArray[Int(arc4random_uniform(UInt32(curPalette.colorNum)))]
    }
    
    override func initializeAttributes() {
        self.size = self.texture!.size()
        super.initializeAttributes()
    }
    
    override func initializePhysics() {
        super.initializePhysics()
        
        self.physicsBody!.allowsRotation = true
        self.physicsBody!.collisionBitMask =
            ColorBubbleCategory | MovingBubbleCategory
    }
    
    override func initializeActions() {
        super.initializeActions()

        self.changeColorAnimAction = SKAction.repeatActionForever(
            SKAction.animateWithTextures(self.sprites_Idle, timePerFrame: 0.1)
        )
        self.changeSizeAnimAction = SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.scaleBy(1.1, duration: 0.5),
                SKAction.scaleBy(0.9, duration: 0.5)
                ])
        )
        
        self.blowUpAnimAction = SKAction.sequence([
            SKAction.runBlock({
                self.runAction(SKAction.group([
                    SKAction.scaleTo(self.getScaleByLevel(self.level), duration: 0.7),
                    SKAction.animateWithTextures(self.sprites_Scale, timePerFrame: 0.07)
                    ]))
            }),
            SKAction.waitForDuration(0.7),
            SKAction.runBlock({
                var velocity = self.physicsBody!.velocity
                self.initializePhysics()
                self.physicsBody!.categoryBitMask = MovingBubbleCategory
                self.physicsBody!.contactTestBitMask = BombBubbleCategory
                // Slightly decrease the speed as the size has been increased.
                self.physicsBody!.velocity = CGVectorMake(velocity.dx * 0.8, velocity.dy * 0.8)
                
                var scene = self.scene! as! GameScene
                if (scene.gameStarted) {
                    self.userInteractionEnabled = true
                }
                
                self.runAction(self.changeColorAnimAction,withKey: "changeColor")
                self.runAction(self.changeSizeAnimAction, withKey: "changeSize")
            })
        ])
        
        
        self.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.fadeAlphaTo(0.8, duration: 0.05),
                SKAction.scaleTo(1 - 0.1 * CGFloat(level), duration: 0.2)
            ]),
            SKAction.runBlock({
                self.physicsBody!.categoryBitMask = MovingBubbleCategory
                self.physicsBody!.contactTestBitMask = BombBubbleCategory
                
                self.runAction(self.changeColorAnimAction,withKey: "changeColor")
                self.runAction(self.changeSizeAnimAction, withKey: "changeSize")
            })
        ]))
    }
    
    override func update() {
        super.update()
    }
    
    override func pop() {
        self.removeActionForKey("levelDown")
        self.removeActionForKey("changeColor")
        self.removeActionForKey("changeSize")
        self.zPosition = -1
        self.alpha = 0.5
        self.isAlive = false
        self.userInteractionEnabled = false
        self.physicsBody!.collisionBitMask = 0x0
        self.physicsBody!.categoryBitMask = PaintBubbleCategory
        self.physicsBody!.contactTestBitMask = ColorBubbleCategory

        var temp:SKPhysicsBody = self.physicsBody!
        temp.dynamic = false
        self.physicsBody = temp
        
        var colorIndex:Int = find(spriteArray, self.texture!)!
        self.paintColor = BubbleColor.getColorByIndex(colorIndex)
        self.sprites_Pop = Array(Bubble.getAnimationFramesForColorBubble(paintColor!)[0..<4])
        self.texture = sprites_Pop[0]
        
        var scale:CGFloat = 1.5 + CGFloat(self.level) / 8
        
        if (userSettings.sfxEnabled) {
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(0.15),
                paintSfxAction
            ]))
        }
        
        self.runAction(SKAction.sequence([
            SKAction.scaleTo(scale, duration: 0.15),
            SKAction.runBlock({
                self.physicsBody = SKPhysicsBody(circleOfRadius: self.frame.size.width/2 * 0.8)
                self.physicsBody!.categoryBitMask = PaintBubbleCategory
                self.physicsBody!.contactTestBitMask = ColorBubbleCategory
                self.physicsBody!.collisionBitMask = 0x0
            }),
            SKAction.runBlock({
                Utility.shake(self, amplitude: 8, duration: 0.5, frequency: 20)
            }),
            SKAction.waitForDuration(0.6),
            SKAction.animateWithTextures(sprites_Pop, timePerFrame: 0.05),
            SKAction.fadeOutWithDuration(0.5)
            ]),
            completion: {
                self.removeFromParent()
            }
        )
        
        if (self.scene! is GameScene && gameCenterEnabled) {
            // Check Total Paint Bubble achievement
            userSettings.achievementsTrack[1]++
            switch(userSettings.achievementsTrack[1]) {
            case 10..<100:
                GameCenterHelper.reportAchievement(AchievementID_TPB10)
            case 100..<500:
                GameCenterHelper.reportAchievement(AchievementID_TPB100)
            case 500..<1000:
                GameCenterHelper.reportAchievement(AchievementID_TPB500)
            default:
                if (userSettings.achievementsTrack[1] >= 1000) {
                    GameCenterHelper.reportAchievement(AchievementID_TPB1000)
                }
            }
        }
        
        super.pop()
    }
    
    override func decreaseOneLevel() {
        if (self.level > 0) {
            // Remove interactions
            self.removeActionForKey("changeColor")
            self.removeActionForKey("changeSize")
            self.setScale(1 - 0.1 * CGFloat(level))
            self.userInteractionEnabled = false
            self.physicsBody!.categoryBitMask = 0x0
            self.physicsBody!.contactTestBitMask = 0x0
            
            self.level--      
            self.runAction(blowUpAnimAction, withKey: "levelDown")
        }
    }
}
