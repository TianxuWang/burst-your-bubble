//
//  CustomizeScene.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/10/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

var moneySfxAction:SKAction!
var digitSfxAction:SKAction!

class CustomizeScene: SKScene {
    
    var soundIndex:Int!
    var paletteIndex:Int!
    
    var titleLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var backBubble:ColorBubble!
    var backIcon:SKSpriteNode!
    
    var soundLabel:SKLabelNode!
    var soundNameLabel:SKLabelNode!
    var soundPagerLabel:SKLabelNode!
    var soundSelectedArea_Left:SKSpriteNode!
    var soundSelectedArea_Right:SKSpriteNode!
    var soundSelectLeftLabel:SKLabelNode!
    var soundSelectRightLabel:SKLabelNode!
    var soundPriceLabel:SKLabelNode!
    
    var paletteLabel:SKLabelNode!
    var paletteNameLabel:SKLabelNode!
    var palettePagerLabel:SKLabelNode!
    var paletteDisplayImg:SKSpriteNode!
    var paletteSelectedArea_Left:SKSpriteNode!
    var paletteSelectedArea_Right:SKSpriteNode!
    var paletteSelectLeftLabel:SKLabelNode!
    var paletteSelectRightLabel:SKLabelNode!
    var palettePriceLabel:SKLabelNode!
    
    var bgColorLabel:SKLabelNode!
    var bgColorSelectedArea_Black:SKSpriteNode!
    var bgColorSelectedArea_White:SKSpriteNode!
    var bgColorSelectBlackLabel:SKLabelNode!
    var bgColorSelectWhiteLabel:SKLabelNode!
    var selectBubble_BgColor:ColorBubble!
    
    var moneyText:MoneyText!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        
        soundIndex = curSoundTheme.id
        paletteIndex = curPalette.id
        
        // Title
        titleLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 180)
        titleLabel.fontSize = 40
        titleLabel.fontColor = Color_Gray
        titleLabel.text = "Customize"
        self.addChild(titleLabel)
        
        // Money Text
        moneyText = MoneyText(position: CGPointMake(self.frame.size.width - 13, self.frame.size.height - 30))
        self.addChild(moneyText)
        
        // Background Color
        bgColorLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        bgColorLabel.position = CGPointMake(self.frame.size.width / 2 - 120, self.frame.size.height / 2 + 110)
        bgColorLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        bgColorLabel.fontSize = 20
        bgColorLabel.fontColor = Color_Gray
        bgColorLabel.text = "Theme"
        bgColorLabel.name = "bgColorLabel"
        self.addChild(bgColorLabel)
        
        bgColorSelectedArea_Black = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: CGSize(width: 40, height: 40))
        bgColorSelectedArea_Black.position = CGPointMake(self.frame.size.width / 2 - 40, self.frame.size.height / 2 + 70)
        bgColorSelectedArea_Black.name = "bgColorSelectedArea_Black"
        bgColorSelectBlackLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        bgColorSelectedArea_Black.addChild(bgColorSelectBlackLabel)
        bgColorSelectBlackLabel.name = "bgColorSelectBlackLabel"
        bgColorSelectBlackLabel.position = CGPointMake(0, 0)
        bgColorSelectBlackLabel.fontSize = 17
        bgColorSelectBlackLabel.fontColor = Color_Gray
        bgColorSelectBlackLabel.text = "Dark"
        self.addChild(bgColorSelectedArea_Black)
        
        bgColorSelectedArea_White = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: CGSize(width: 40, height: 40))
        bgColorSelectedArea_White.position = CGPointMake(self.frame.size.width / 2 + 40, self.frame.size.height / 2 + 70)
        bgColorSelectedArea_White.name = "bgColorSelectedArea_White"
        bgColorSelectWhiteLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        bgColorSelectedArea_White.addChild(bgColorSelectWhiteLabel)
        bgColorSelectWhiteLabel.name = "bgColorSelectWhiteLabel"
        bgColorSelectWhiteLabel.position = CGPointMake(0, 0)
        bgColorSelectWhiteLabel.fontSize = 17
        bgColorSelectWhiteLabel.fontColor = Color_Gray
        bgColorSelectWhiteLabel.text = "Light"
        self.addChild(bgColorSelectedArea_White)
        
        var selectedLabel = getBgColorLabel(userSettings.bgColorBlack)
        selectedLabel.fontColor = SKColor.whiteColor()
        var selectedBubblePosition = selectedLabel.parent!.position
        selectBubble_BgColor = BubbleFactory.spawnColorBubbleWithRandomColor(5, position: CGPointMake(selectedBubblePosition.x, selectedBubblePosition.y + 6))
        selectBubble_BgColor.name = "selectBubble_BgColor"
        selectBubble_BgColor.userInteractionEnabled = false
        selectBubble_BgColor.zPosition = -1
        self.addChild(selectBubble_BgColor)
        
        // Sound Theme
        soundLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        soundLabel.position = CGPointMake(self.frame.size.width / 2 - 120, self.frame.size.height / 2 + 10)
        soundLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        soundLabel.fontSize = 20
        soundLabel.fontColor = Color_Gray
        soundLabel.text = "Music Pack"
        soundLabel.name = "soundLabel"
        self.addChild(soundLabel)
        
        soundNameLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        soundNameLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 22)
        soundNameLabel.fontColor = Color_Gray
        soundNameLabel.fontSize = 18
        soundNameLabel.text = curSoundTheme.name
        soundNameLabel.name = "soundNameLabel"
        self.addChild(soundNameLabel)
        
        soundSelectedArea_Left = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: CGSize(width: 40, height: 40))
        soundSelectedArea_Left.position = CGPointMake(self.frame.size.width / 2 - 110, self.frame.size.height / 2 - 25)
        soundSelectedArea_Left.name = "soundSelectedArea_Left"
        soundSelectLeftLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        soundSelectedArea_Left.addChild(soundSelectLeftLabel)
        soundSelectLeftLabel.name = "soundSelectLeftLabel"
        soundSelectLeftLabel.position = CGPointMake(0, 0)
        soundSelectLeftLabel.fontSize = 20
        soundSelectLeftLabel.fontColor = Color_Gray
        soundSelectLeftLabel.text = "<"
        self.addChild(soundSelectedArea_Left)
        
        soundSelectedArea_Right = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: CGSize(width: 40, height: 40))
        soundSelectedArea_Right.position = CGPointMake(self.frame.size.width / 2 + 110, self.frame.size.height / 2 - 25)
        soundSelectedArea_Right.name = "soundSelectedArea_Right"
        soundSelectRightLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        soundSelectedArea_Right.addChild(soundSelectRightLabel)
        soundSelectRightLabel.name = "soundSelectRightLabel"
        soundSelectRightLabel.position = CGPointMake(0, 0)
        soundSelectRightLabel.fontSize = 20
        soundSelectRightLabel.fontColor = Color_Gray
        soundSelectRightLabel.text = ">"
        self.addChild(soundSelectedArea_Right)
        
        soundPagerLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        soundPagerLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 55)
        soundPagerLabel.fontColor = Color_Gray
        soundPagerLabel.fontSize = 13
        soundPagerLabel.name = "soundPagerLabel"
        self.addChild(soundPagerLabel)
        
        soundPriceLabel = SKLabelNode(fontNamed: FontName_Money)
        soundPriceLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 24)
        soundPriceLabel.fontColor = Color_Gold
        soundPriceLabel.fontSize = 20
        soundPriceLabel.name = "soundPriceLabel"
        
        // Palette
        paletteLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        paletteLabel.position = CGPointMake(self.frame.size.width / 2 - 120, self.frame.size.height / 2 - 90)
        paletteLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        paletteLabel.fontSize = 20
        paletteLabel.fontColor = Color_Gray
        paletteLabel.text = "Palette"
        paletteLabel.name = "paletteLabel"
        self.addChild(paletteLabel)
        
        paletteNameLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        paletteNameLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 110)
        paletteNameLabel.fontColor = Color_Gray
        paletteNameLabel.fontSize = 13
        paletteNameLabel.name = "paletteNameLabel"
        paletteNameLabel.text = curPalette.name
        self.addChild(paletteNameLabel)
        
        palettePagerLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        palettePagerLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 170)
        palettePagerLabel.fontSize = 13
        palettePagerLabel.fontColor = Color_Gray
        palettePagerLabel.name = "palettePagerLabel"
        self.addChild(palettePagerLabel)
        
        paletteDisplayImg = SKSpriteNode(texture: curPalette.displayImg, color: SKColor.clearColor(), size: curPalette.displayImg.size())
        paletteDisplayImg.name = "paletteDisplayImg"
        paletteDisplayImg.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 130)
        self.addChild(paletteDisplayImg)
        
        paletteSelectedArea_Left = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: CGSize(width: 40, height: 40))
        paletteSelectedArea_Left.position = CGPointMake(self.frame.size.width / 2 - 110, self.frame.size.height / 2 - 140)
        paletteSelectedArea_Left.name = "paletteSelectedArea_Left"
        paletteSelectLeftLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        paletteSelectedArea_Left.addChild(paletteSelectLeftLabel)
        paletteSelectLeftLabel.name = "paletteSelectLeftLabel"
        paletteSelectLeftLabel.position = CGPointMake(0, 0)
        paletteSelectLeftLabel.fontSize = 20
        paletteSelectLeftLabel.fontColor = Color_Gray
        paletteSelectLeftLabel.text = "<"
        self.addChild(paletteSelectedArea_Left)
        
        paletteSelectedArea_Right = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: CGSize(width: 40, height: 40))
        paletteSelectedArea_Right.position = CGPointMake(self.frame.size.width / 2 + 110, self.frame.size.height / 2 - 140)
        paletteSelectedArea_Right.name = "paletteSelectedArea_Right"
        paletteSelectRightLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        paletteSelectedArea_Right.addChild(paletteSelectRightLabel)
        paletteSelectRightLabel.name = "paletteSelectRightLabel"
        paletteSelectRightLabel.position = CGPointMake(0, 0)
        paletteSelectRightLabel.fontSize = 20
        paletteSelectRightLabel.fontColor = Color_Gray
        paletteSelectRightLabel.text = ">"
        self.addChild(paletteSelectedArea_Right)
        
        palettePriceLabel = SKLabelNode(fontNamed: FontName_Money)
        palettePriceLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 - 139)
        palettePriceLabel.fontColor = Color_Gold
        palettePriceLabel.fontSize = 20
        palettePriceLabel.name = "palettePriceLabel"
        
        // Back button
        backBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            2,
            position: CGPointMake(0, self.frame.size.height)
        )
        backBubble.userInteractionEnabled = false
        backBubble.name = "backBubble"
        self.addChild(backBubble)
        
        backIcon = SKSpriteNode(imageNamed: "back.png")
        backIcon.name = "backIcon"
        backIcon.position = CGPointMake(18, self.frame.size.height - 18)
        backIcon.setScale(0.05)
        backIcon.alpha = 0
        self.addChild(backIcon)
        backIcon.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(0.28, duration: 0.2),
                    SKAction.scaleTo(0.23, duration: 0.02)
                ])
            ])
        ]))
        
        ViewController.playTrackAtIndex(userSettings.selectedMusicThemeId)
        bgMusicPlayer.play()
    }
    
    override func update(currentTime: NSTimeInterval) {
        soundPagerLabel.text = "\(soundIndex + 1) of \(SoundThemeNum)"
        palettePagerLabel.text = "\(paletteIndex + 1) of \(PaletteNum)"
        
        moneyText.update()
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil) {
                if (node!.name == "bgColorSelectedArea_Black" || node!.name == "bgColorSelectBlackLabel") {
                    self.updateBgColor(true)
                }
                else if (node!.name == "bgColorSelectedArea_White" || node!.name == "bgColorSelectWhiteLabel") {
                    self.updateBgColor(false)
                }
                else if (node!.name == "soundSelectedArea_Left" || node!.name == "soundSelectLeftLabel") {
                    Utility.shake(soundSelectedArea_Left, amplitude: 3, duration: 0.5, frequency: 20)
                    soundIndex = soundIndex > 0 ? soundIndex - 1 : SoundThemeNum - 1
                    self.updateSoundTheme()
                }
                else if (node!.name == "soundSelectedArea_Right" || node!.name == "soundSelectRightLabel") {
                    Utility.shake(soundSelectedArea_Right, amplitude: 3, duration: 0.5, frequency: 20)
                    soundIndex = soundIndex < SoundThemeNum - 1 ? soundIndex + 1 : 0
                    self.updateSoundTheme()
                }
                else if (node!.name == "paletteSelectedArea_Left" || node!.name == "paletteSelectLeftLabel") {
                    Utility.shake(paletteSelectedArea_Left, amplitude: 3, duration: 0.5, frequency: 20)
                    paletteIndex = paletteIndex > 0 ? paletteIndex - 1 : PaletteNum - 1
                    self.updatePalette()
                }
                else if (node!.name == "paletteSelectedArea_Right" || node!.name == "paletteSelectRightLabel") {
                    Utility.shake(paletteSelectedArea_Right, amplitude: 3, duration: 0.5, frequency: 20)
                    paletteIndex = paletteIndex < PaletteNum - 1 ? paletteIndex + 1 : 0
                    self.updatePalette()
                }
                else if (node!.name == "soundPriceLabel") {
                    if (money! >= soundThemes[soundIndex].price) {
                        self.unlockSoundTheme()
                    }
                    else {
                        Utility.shake(soundPriceLabel, amplitude: 4, duration: 0.5, frequency: 20)
                        if (userSettings.sfxEnabled) {
                            soundPriceLabel.runAction(buttonSfxAction)
                        }
                    }
                }
                else if (node!.name == "palettePriceLabel") {
                    if (money! >= palettes[paletteIndex].price) {
                        self.unlockPalette()
                    }
                    else {
                        Utility.shake(palettePriceLabel, amplitude: 4, duration: 0.5, frequency: 20)
                        if (userSettings.sfxEnabled) {
                            palettePriceLabel.runAction(buttonSfxAction)
                        }
                    }
                }
                else if (node!.name == "backBubble" || node!.name == "backIcon") {
                    NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
                    
                    if (userSettings.sfxEnabled) {
                        backBubble.runAction(buttonSfxAction)
                    }
                    runBackAction(self.backBubble)
                }
            }
        }
    }
    
    func runBackAction(bubble:ColorBubble) {
        self.runAction(SKAction.sequence([
            SKAction.runBlock({
                Utility.shake(bubble, amplitude: 4, duration: 0.5, frequency: 20)
            }),
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var menuScene:GameMenuScene = GameMenuScene(size: self.size)
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                self.view!.presentScene(menuScene, transition: transition)
            })
        ]))
    }
    func updateSoundTheme() { 
        var selectedSound = soundThemes[soundIndex]
        soundNameLabel.text = selectedSound.name
        soundPriceLabel.removeFromParent()
        soundPriceLabel.text = "• \(selectedSound.price)"
        
        if (userSettings.sfxEnabled) {
            soundSelectRightLabel.runAction(selectedSound.scaleSfxActions[10])
        }
        ViewController.playTrackAtIndex(selectedSound.id)
        
        if userSettings.musicThemeUnlock[soundIndex] {
            userSettings.selectedMusicThemeId = soundIndex
            SoundTheme.update()
            soundNameLabel.hidden = false
        }
        else {
            soundNameLabel.hidden = true
            self.addChild(soundPriceLabel)
        }
    }
    
    func unlockSoundTheme() {
        if (userSettings.sfxEnabled) {
            self.runAction(successSfxAction)
        }
        
        money! -= soundThemes[soundIndex].price
        NSUserDefaults.standardUserDefaults().setObject(money!, forKey: "money")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        userSettings.musicThemeUnlock[soundIndex] = true
        NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
        
        userSettings.selectedMusicThemeId = soundIndex
        SoundTheme.update()
        
        soundNameLabel.hidden = false
        soundPriceLabel.removeFromParent()
    }
    
    func updatePalette() {
        var selectedPalette = palettes[paletteIndex]
        paletteNameLabel.text = selectedPalette.name
        paletteDisplayImg.texture! = selectedPalette.displayImg
        palettePriceLabel.removeFromParent()
        palettePriceLabel.text = "• \(selectedPalette.price)"
        
        if (userSettings.sfxEnabled) {
            paletteSelectRightLabel.runAction(bubbleBonusSfxActions[10])
        }
        
        if userSettings.paletteUnlock[paletteIndex] {
            userSettings.selectedPaletteId = paletteIndex
            Palette.update()
            paletteDisplayImg.hidden = false
        }
        else {
            paletteDisplayImg.hidden = true
            self.addChild(palettePriceLabel)
        }
    }
    
    func unlockPalette() {
        if (userSettings.sfxEnabled) {
            self.runAction(successSfxAction)
        }

        money! -= palettes[paletteIndex].price
        NSUserDefaults.standardUserDefaults().setObject(money!, forKey: "money")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        userSettings.paletteUnlock[paletteIndex] = true
        NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
        
        userSettings.selectedPaletteId = paletteIndex
        Palette.update()
        
        paletteDisplayImg.hidden = false
        palettePriceLabel.removeFromParent()
    }
    
    func updateBgColor(isBlack:Bool) {
        if (userSettings.bgColorBlack != isBlack) {
            getBgColorLabel(userSettings.bgColorBlack).fontColor = Color_Gray
            userSettings.bgColorBlack = isBlack
            getBgColorLabel(isBlack).fontColor = SKColor.whiteColor()
            
            var pos = getBgColorLabel(isBlack).parent!.position
            selectBubble_BgColor.shuffleColor()
            selectBubble_BgColor.position = CGPointMake(pos.x, pos.y + 6)
            
            self.backgroundColor = isBlack ? Color_Black : Color_White
        }
        else {
            Utility.shake(selectBubble_BgColor, amplitude: 4, duration: 0.5, frequency: 20)
        }
        
        if (userSettings.sfxEnabled) {
            selectBubble_BgColor.runAction(bubbleBonusSfxActions[10])
        }
    }
    
    func getBgColorLabel(isBlack: Bool) -> SKLabelNode {
        return isBlack ? bgColorSelectBlackLabel : bgColorSelectWhiteLabel
    }
}
