//
//  GameViewController.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/27/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import UIKit
import SpriteKit
import iAd
import GameKit
import AVFoundation

let FontName_DefaultLight:String = "Noteworthy-Light"
let FontName_DefaultBold:String = "Noteworthy-Bold"
let FontName_Symbol:String = "AvenirNext-Bold"
let FontName_SymbolHeavy:String = "AvenirNext-Heavy"
let FontName_SymbolLight:String = "MarkerFelt-Thin"
let FontName_SymbolBold:String = "MarkerFelt-Wide"
let FontName_Money:String = "ChalkboardSE-Bold"
let FontName_Info:String = "Baskerville-BoldItalic"
let Color_Gray:SKColor = SKColor.lightGrayColor()
let Color_White = SKColor(red: 254/255, green: 249/255, blue: 240/255, alpha: 1.0)
let Color_Black = SKColor(red: 41/255, green: 44/255, blue: 55/255, alpha: 1.0)
let Color_Green:SKColor = SKColor(red: 0, green: 1.0, blue: 128/255, alpha: 1.0)
let Color_Gold:SKColor = SKColor(red: 251/255, green: 184/255, blue: 41/255, alpha: 1.0)
let AchievementTrackedNum = 3

var gameCenterEnabled:Bool = true
var achievementMap:NSMutableDictionary!
var isFirstLaunch:Bool = false
var userSettings:UserSettings!
var bgMusicPlayer:AVQueuePlayer!
var menuBgMusic:AVPlayerItem!
var soundThemes:[SoundTheme] = [SoundTheme]()
var palettes:[Palette] = [Palette]()
var curPalette:Palette!
var curSoundTheme:SoundTheme!
var money:Int?

class ViewController: UIViewController, ADBannerViewDelegate, GKGameCenterControllerDelegate {
    
    var adBannerView: ADBannerView!
    var shouldDisplayBannerAds: Bool = true
    var skView:SKView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.authenticateLocalPlayer()
        self.loadLocalUserSettings()
        self.loadMoney()
        
        skView = self.view as! SKView
        var preloadScene:SKScene = GamePreloadScene(size: skView.bounds.size)
        preloadScene.scaleMode = SKSceneScaleMode.AspectFill
        
        skView.presentScene(preloadScene)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: "loadAdBanner", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: "hideAdBanner", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: "showAdBanner", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: "openGameCenter", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: "saveSettings", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: "pauseApp", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func bannerViewDidLoadAd(banner: ADBannerView!) {
        if (shouldDisplayBannerAds) {
            adBannerView.hidden = false
        }
    }
    
    func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
        adBannerView.hidden = true
    }
    
    func bannerViewActionShouldBegin(banner: ADBannerView!, willLeaveApplication willLeave: Bool) -> Bool {
        var isGameScene = skView.scene! is GameScene
        if (!isGameScene) {
            skView.scene!.paused = true
        }
        
        bgMusicPlayer.pause()
        return true
    }
    
    func bannerViewActionDidFinish(banner: ADBannerView!) {
        skView.scene!.paused = false
        var isGameScene = skView.scene! is GameScene
        if (!isGameScene) {
            bgMusicPlayer.play()
        }
    }
    
    func loadAds() {
        adBannerView = ADBannerView(frame: CGRect.zeroRect)
        adBannerView.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.size.height - adBannerView.frame.size.height / 2)
        adBannerView.delegate = self
        adBannerView.hidden = true
        view.addSubview(adBannerView)
    }
    
    func handleNotification(notification: NSNotification) {
        switch(notification.name) {
            case "loadAdBanner":
                self.loadAds()
            case "showAdBanner":
                self.adBannerView.hidden = false
                self.shouldDisplayBannerAds = true
            case "hideAdBanner":
                self.adBannerView.hidden = true
                self.shouldDisplayBannerAds = false
            case "openGameCenter":
                self.openGameCenter()
            case "saveSettings":
                self.saveLocalUserSettings()
            case "pauseApp":
                if (skView.scene! is GameScene) {
                    var gameScene:GameScene = skView.scene! as! GameScene
                    gameScene.gamePause()
                }
            case AVPlayerItemDidPlayToEndTimeNotification:
                bgMusicPlayer.currentItem.seekToTime(kCMTimeZero)
            default:
                return
        }
    }
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController!) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: {() -> Void in
            self.skView.scene!.paused = false
            bgMusicPlayer.play()
        })
    }
    
    func openGameCenter() {
        skView.scene!.paused = true
        bgMusicPlayer.pause()
        
        let localPlayer = GKLocalPlayer.localPlayer()
        
        if (localPlayer.authenticated && gameCenterEnabled) {
            var gameCenterController:GKGameCenterViewController = GKGameCenterViewController()
            gameCenterController.gameCenterDelegate = self
            gameCenterController.viewState = GKGameCenterViewControllerState.Leaderboards
            self.presentViewController(gameCenterController, animated: true, completion: nil)
        }
    }
    
    func authenticateLocalPlayer() {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            if (viewController != nil) {
                self.presentViewController(viewController, animated: true, completion: nil)
            }
            else if (localPlayer.authenticated) {
                println("Local player already authenticated")
                gameCenterEnabled = true
                GameCenterHelper.loadAchievements()
                //GameCenterHelper.resetAchievements()
            }
            else {
                gameCenterEnabled = false
                println("Local player could not be authenticated, disabling GameCenter")
            }
        }
    }
    
    func loadLocalUserSettings() {
        var settings:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var encodedObj:NSData? = settings.objectForKey("UserSettings") as! NSData?
        if (encodedObj != nil) {
            userSettings = NSKeyedUnarchiver.unarchiveObjectWithData(encodedObj!) as! UserSettings
        }
        else {
            userSettings = UserSettings()
        }
        
        while(userSettings.achievementsTrack.count < AchievementTrackedNum) {
            userSettings.achievementsTrack.append(0)
        }
        
        while(userSettings.musicThemeUnlock.count < SoundThemeNum) {
            userSettings.musicThemeUnlock.append(false)
        }
        
        while(userSettings.paletteUnlock.count < PaletteNum) {
            userSettings.paletteUnlock.append(false)
        }
    }
    
    func saveLocalUserSettings() {
        var settings:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var encodedObj:NSData = NSKeyedArchiver.archivedDataWithRootObject(userSettings)
        settings.setObject(encodedObj, forKey: "UserSettings")
        settings.synchronize()
    }
    
    func loadMoney() {
        var m = NSUserDefaults.standardUserDefaults().objectForKey("money") as! Int?
        money = m == nil ? 0 : m!
    }
    
    class func playTrackAtIndex(index: Int) {
        bgMusicPlayer.removeAllItems()
        
        var item:AVPlayerItem = soundThemes[index].bgMusic
        if (bgMusicPlayer.canInsertItem(item, afterItem: nil)) {
            item.seekToTime(kCMTimeZero)
            bgMusicPlayer.insertItem(item, afterItem: nil)
        }
    }
    
    class func playMenuBgMusic() {
        if (bgMusicPlayer.currentItem != menuBgMusic) {
            bgMusicPlayer.removeAllItems()
            if (bgMusicPlayer.canInsertItem(menuBgMusic, afterItem: nil)) {
                menuBgMusic.seekToTime(kCMTimeZero)
                bgMusicPlayer.insertItem(menuBgMusic, afterItem: nil)
            }
        }
    }
}
