//
//  ScoreCalculator.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/29/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit
import GameKit

class ScoreCalculator {
    
    var sameColorBonus:Int
    var maxDifference:Int
    
    var totalScore:Int
    var bonusScore:Int
    var prevColor:BubbleColor!
    var successiveCount:Int
    var bonusSfxIndex:Int
    
    var roundNum:Int!
    
    init() {
        self.totalScore = 0
        self.bonusScore = 0
        self.successiveCount = 0
        self.prevColor = nil
        self.bonusSfxIndex = 0
        
        maxDifference = Int(curPalette.colorNum / 2)
        sameColorBonus = curPalette.colorNum % 2 == 0 ? curPalette.colorNum : curPalette.colorNum - 1
    }
    
    func addScore(bubble: ColorBubble, updatePreColor: Bool) {
        let color:BubbleColor = bubble.colorCategory!
        let basicScore:Int = bubble.basicScore
        var bonusScoreThisTime:Int = 0
        var bonusSfxIndexThisTime:Int = 0
        var notIterateToNextTime:Bool = false
        var isInGame:Bool = bubble.scene! is GameScene
        
        if (prevColor == nil) {
            bonusScore = 0
        }
        else if (color == prevColor) {
            bonusScore += sameColorBonus
            successiveCount++
            bonusSfxIndex = min(bonusSfxIndex + 1, BubblePopScaleSfxNum - 1)
            
            // Increase the size of a random bubble
            if (isInGame) {
                var scene:GameScene = bubble.scene! as! GameScene
                var bubbles = scene.bubblePool
                var boundary = scene.colorBoundary
                
                boundary.pulse()
                
                var chosenBubble:Bubble?
                while (bubbles.count > 0) {
                    var rand = Int(arc4random_uniform(UInt32(bubbles.count)))
                    chosenBubble = bubbles[rand]
                    if (chosenBubble!.isAbleToIncreaseSize()) {
                        break
                    }
                    else {
                        // remove this bubble from pool if it is already in the largest size.
                        bubbles.removeAtIndex(rand)
                    }
                }
                if (chosenBubble != nil) {
                    chosenBubble!.decreaseOneLevel()
                }
            }
        }
        else {
            var colorDiff = color.colorDifferenceTo(prevColor)
            var remainFactor = 1.0 - CGFloat(colorDiff) / CGFloat(maxDifference)
            
            bonusScoreThisTime = bonusScore == 0 ?
                         sameColorBonus - colorDiff * 2 :
                         Int(CGFloat(bonusScore) * remainFactor)
            
            bonusSfxIndexThisTime = Int(CGFloat(bonusSfxIndex) * remainFactor)
            
            if (updatePreColor) {
                successiveCount = 0
                bonusScore = bonusScoreThisTime
                bonusSfxIndex = bonusSfxIndexThisTime
            }
            else {
                notIterateToNextTime = true
            }
        }
        
        var score = basicScore + (notIterateToNextTime ? bonusScoreThisTime : bonusScore)
        var scoreText = "+\(score)"
        // In Endless mode, only add score when changing color.
        if (gameMode == GameMode.Endless && prevColor != nil && color == prevColor) {
            scoreText = String(score)
        }
        else {
            totalScore += score
            // check Single Tap Achievement
            if (isInGame && gameCenterEnabled) {
                switch (score) {
                case 100..<500:
                    GameCenterHelper.reportAchievement(AchievementID_ST100)
                case 500..<1000:
                    GameCenterHelper.reportAchievement(AchievementID_ST500)
                case 1000..<1500:
                    GameCenterHelper.reportAchievement(AchievementID_ST1000)
                case 1500..<2000:
                    GameCenterHelper.reportAchievement(AchievementID_ST1500)
                case 2000..<3000:
                    GameCenterHelper.reportAchievement(AchievementID_ST2000)
                default:
                    if (score >= 3000) {
                        GameCenterHelper.reportAchievement(AchievementID_ST3000)
                    }
                }
            }
        }
        
        // Create float text
        var floatScoreText:FloatText = FloatText(
            text: scoreText,
            position: bubble.position,
            level: (prevColor != nil && color == prevColor) ? successiveCount : 0,
            color: color
        )
        bubble.parent!.addChild(floatScoreText)
        if (!notIterateToNextTime && bonusSfxIndex == BubblePopScaleSfxNum - 1) {
            floatScoreText.changeColor()
        }
        if (userSettings.sfxEnabled) {
            bubble.runAction(bubbleBonusSfxActions[notIterateToNextTime ? bonusSfxIndexThisTime : bonusSfxIndex])
        }
        
        if (updatePreColor) {
            prevColor = color
        }
    }
    
    // Save this score if needed, return high score before saving.
    func saveScore() -> Int? {
        var score = gameMode! == GameMode.Challenge ? roundNum : totalScore
        let leaderboardID = gameMode!.getLeaderboardID()
        var highScore:Int? = NSUserDefaults.standardUserDefaults().objectForKey(leaderboardID) as! Int?
        if (highScore == nil || score > highScore) {
            NSUserDefaults.standardUserDefaults().setObject(score, forKey: leaderboardID)
            NSUserDefaults.standardUserDefaults().synchronize()
        }

        // Push score to leaderboard
        if (gameCenterEnabled && GKLocalPlayer.localPlayer().authenticated) {
            var scoreReporter = GKScore(leaderboardIdentifier: leaderboardID)
            scoreReporter.value = Int64(score)
            var scoreArray:[GKScore] = [scoreReporter]
            GKScore.reportScores(scoreArray, withCompletionHandler: {(error: NSError!) -> Void in
                if (error != nil) {
                    println(error.localizedDescription)
                }
            })
        }
        
        return highScore
    }
}