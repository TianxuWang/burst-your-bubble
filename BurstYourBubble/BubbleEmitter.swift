//
//  ParticleEmitter.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/22/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class BubbleEmitter {
    
    var parent:SKNode!
    var position:CGPoint!
    var level:Int!
    var directionX:Bool?
    var directionY:Bool?
    var interval:NSTimeInterval!
    var lastTime:NSTimeInterval!
    var rangeTime:NSTimeInterval!
    var name:String!
    var bubbleCount:Int!
    
    init(node:SKNode, name:String, level:Int, pos:CGPoint, directionX:Bool?, directionY:Bool?, interval:NSTimeInterval, lastTime:NSTimeInterval, rangeTime:NSTimeInterval) {
        self.parent = node
        self.name = name
        self.position = pos
        self.level = level
        self.directionX = directionX
        self.directionY = directionY
        self.interval = interval
        self.lastTime = lastTime
        self.rangeTime = rangeTime
        self.bubbleCount = 20
    }
    
    func emit() {
        self.parent.runAction(SKAction.repeatActionForever(SKAction.sequence([
            SKAction.runBlock({
                var bubble = BubbleFactory.spawnColorBubbleWithRandomColor(self.level, position: self.position)
                bubble.userInteractionEnabled = false
                self.parent.addChild(bubble)
                if (self.directionX != nil && self.directionY != nil) {
                    bubble.applyRandomImpulseOnDirection(self.directionX!, directionY: self.directionY!)
                }
                else if (self.directionX != nil) {
                    bubble.applyImpulseOnX(self.directionX!)
                }
                else if (self.directionY != nil) {
                    bubble.applyImpulseOnY(self.directionY!)
                }
                
                bubble.runAction(
                    SKAction.waitForDuration(self.lastTime, withRange: self.rangeTime),
                    completion: {
                        bubble.pop()
                    }
                )
            }),
            SKAction.waitForDuration(self.interval)
        ])), withKey: self.name)
    }
    
    func emitOnce() {
        for var i = 0; i < bubbleCount; i++ {
            var bubble = BubbleFactory.spawnColorBubbleWithRandomColor(self.level, position: self.position)
            bubble.userInteractionEnabled = false
            self.parent.addChild(bubble)
            if (self.directionX != nil && self.directionY != nil) {
                bubble.applyRandomImpulseOnDirection(self.directionX!, directionY: self.directionY!)
            }
            else if (self.directionX != nil) {
                bubble.applyImpulseOnX(self.directionX!)
            }
            else if (self.directionY != nil) {
                bubble.applyImpulseOnY(self.directionY!)
            }
            
            bubble.runAction(
                SKAction.waitForDuration(self.lastTime, withRange: self.rangeTime),
                completion: {
                    bubble.pop()
                }
            )
        }
    }
    
    func stop() {
        self.parent.removeActionForKey(self.name)
    }
}
