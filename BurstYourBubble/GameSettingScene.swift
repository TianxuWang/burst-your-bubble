//
//  GameSettingScene.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/21/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import Foundation
import SpriteKit

class GameSettingScene: SKScene {
    
    var titleLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    
    var musicLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var musicEnabledLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var musicBubble:ColorBubble!
    
    var sfxLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var sfxEnabledLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var sfxBubble:ColorBubble!
    
    var vibrationLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var vibrationEnabledLabel:SKLabelNode = SKLabelNode(fontNamed: FontName_DefaultBold)
    var vibrationBubble:ColorBubble!
    
    var backBubble:ColorBubble!
    var backIcon:SKSpriteNode!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size:CGSize) {
        super.init(size: size)
        
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        
        // Title
        titleLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 170)
        titleLabel.fontSize = 40
        titleLabel.fontColor = Color_Gray
        titleLabel.text = "Settings"
        self.addChild(titleLabel)
        
        // Music
        musicLabel.position = CGPointMake(self.frame.size.width / 2 - 10, self.frame.size.height / 2 + 80)
        musicLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        musicLabel.fontSize = 20
        musicLabel.fontColor = Color_Gray
        musicLabel.text = "Music:"
        musicLabel.name = "musicLabel"
        self.addChild(musicLabel)
        
        musicBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width / 2 + 50, self.frame.size.height / 2 + 88)
        )
        musicBubble.name = "musicBubble"
        musicBubble.userInteractionEnabled = false
        self.addChild(musicBubble)
        
        musicEnabledLabel.position = CGPointMake(self.frame.size.width / 2 + 50, self.frame.size.height / 2 + 80)
        musicEnabledLabel.fontSize = 20
        musicEnabledLabel.text = userSettings.bgMusicEnabled ? "ON" : "OFF"
        musicEnabledLabel.name = "musicEnabledLabel"
        self.addChild(musicEnabledLabel)
        Utility.buttonLabelZoomIn(musicEnabledLabel)
        
        // Sfx
        sfxLabel.position = CGPointMake(self.frame.size.width / 2 - 10, self.frame.size.height / 2 + 10)
        sfxLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        sfxLabel.fontSize = 20
        sfxLabel.fontColor = Color_Gray
        sfxLabel.text = "Sound Effect:"
        sfxLabel.name = "sfxLabel"
        self.addChild(sfxLabel)
        
        sfxBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width / 2 + 50, self.frame.size.height / 2 + 18)
        )
        sfxBubble.name = "sfxBubble"
        sfxBubble.userInteractionEnabled = false
        self.addChild(sfxBubble)
        
        sfxEnabledLabel.position = CGPointMake(self.frame.size.width / 2 + 50, self.frame.size.height / 2 + 10)
        sfxEnabledLabel.fontSize = 20
        sfxEnabledLabel.text = userSettings.sfxEnabled ? "ON" : "OFF"
        sfxEnabledLabel.name = "sfxEnabledLabel"
        self.addChild(sfxEnabledLabel)
        Utility.buttonLabelZoomIn(sfxEnabledLabel)
        
        // Vibration
        vibrationLabel.position = CGPointMake(self.frame.size.width / 2 - 10, self.frame.size.height / 2 - 60)
        vibrationLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        vibrationLabel.fontSize = 20
        vibrationLabel.fontColor = Color_Gray
        vibrationLabel.text = "Vibration:"
        vibrationLabel.name = "vibrationLabel"
        self.addChild(vibrationLabel)
        
        vibrationBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width / 2 + 50, self.frame.size.height / 2 - 52)
        )
        vibrationBubble.name = "vibrationBubble"
        vibrationBubble.userInteractionEnabled = false
        self.addChild(vibrationBubble)
        
        vibrationEnabledLabel.position = CGPointMake(self.frame.size.width / 2 + 50, self.frame.size.height / 2 - 60)
        vibrationEnabledLabel.fontSize = 20
        vibrationEnabledLabel.text = userSettings.vibrationEnabled ? "ON" : "OFF"
        vibrationEnabledLabel.name = "vibrationEnabledLabel"
        self.addChild(vibrationEnabledLabel)
        Utility.buttonLabelZoomIn(vibrationEnabledLabel)
        
        // Back button
        backBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            2,
            position: CGPointMake(0, self.frame.size.height)
        )
        backBubble.userInteractionEnabled = false
        backBubble.name = "backBubble"
        self.addChild(backBubble)
        
        backIcon = SKSpriteNode(imageNamed: "back.png")
        backIcon.name = "backIcon"
        backIcon.position = CGPointMake(18, self.frame.size.height - 18)
        backIcon.setScale(0.05)
        backIcon.alpha = 0
        self.addChild(backIcon)
        backIcon.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(0.28, duration: 0.2),
                    SKAction.scaleTo(0.23, duration: 0.02)
                ])
            ])
        ]))
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil) {
                if (node!.name == "musicBubble" || node!.name == "musicEnabledLabel") {
                    if (userSettings.sfxEnabled) {
                        musicBubble.runAction(buttonSfxAction)
                    }
                    Utility.shake(musicBubble, amplitude: 4, duration: 0.5, frequency: 20)
                    musicBubble.shuffleColor()
                    userSettings.bgMusicEnabled = !userSettings.bgMusicEnabled
                }
                else if (node!.name == "sfxBubble" || node!.name == "sfxEnabledLabel") {
                    if (userSettings.sfxEnabled) {
                        sfxBubble.runAction(buttonSfxAction)
                    }
                    Utility.shake(sfxBubble, amplitude: 4, duration: 0.5, frequency: 20)
                    sfxBubble.shuffleColor()
                    userSettings.sfxEnabled = !userSettings.sfxEnabled
                }
                else if (node!.name == "vibrationBubble" || node!.name == "vibrationEnabledLabel") {
                    if (userSettings.sfxEnabled) {
                        vibrationBubble.runAction(buttonSfxAction)
                    }
                    Utility.shake(vibrationBubble, amplitude: 4, duration: 0.5, frequency: 20)
                    vibrationBubble.shuffleColor()
                    userSettings.vibrationEnabled = !userSettings.vibrationEnabled
                }
                else if (node!.name == "backBubble" || node!.name == "backIcon") {
                    NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
                    
                    if (userSettings.sfxEnabled) {
                        backBubble.runAction(buttonSfxAction)
                    }
                    runBackAction(self.backBubble)
                }
            }
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        super.update(currentTime)
        
        bgMusicPlayer.volume = userSettings.bgMusicEnabled ? 1.0 : 0.0
        
        // Update UI
        musicEnabledLabel.text = userSettings.bgMusicEnabled ? "ON" : "OFF"
        sfxEnabledLabel.text = userSettings.sfxEnabled ? "ON" : "OFF"
        vibrationEnabledLabel.text = userSettings.vibrationEnabled ? "ON" : "OFF"
    }
    
    func runBackAction(bubble:ColorBubble) {
        self.runAction(SKAction.sequence([
            SKAction.runBlock({
                Utility.shake(bubble, amplitude: 4, duration: 0.5, frequency: 20)
            }),
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var menuScene:GameMenuScene = GameMenuScene(size: self.size)
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                self.view!.presentScene(menuScene, transition: transition)
            })
        ]))
    }
}
