//
//  BubbleFactory.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/28/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

let BombBubblePercentage:Int = 100
let DyeBubblePercentage:Int = 100
let ColorBubblePercentage:Int = 800

class BubbleFactory {
    
    class func spawnRandomBubble(level:Int, position:CGPoint) -> Bubble {
        let rand = Int(arc4random_uniform(1000))
        
        if (rand <= BombBubblePercentage) {
            return self.spawnBombBubble(level, position: position)
        }
        else if (rand <= (1000 - ColorBubblePercentage)) {
            return self.spawnPaintBubble(level, position: position)
        }
        else {
            return self.spawnColorBubbleWithRandomColor(level, position: position)
        }
    }
    
    class func spawnColorBubbleWithRandomColor(level:Int, position:CGPoint) -> ColorBubble {
        return ColorBubble(level: level, position: position)
    }
    
    class func spawnColorBubbleWithColor(color:BubbleColor, level:Int, position:CGPoint) -> ColorBubble {
        return ColorBubble(color: color, level: level, position: position)
    }
    
    class func spawnPaintBubble(level:Int, position:CGPoint) -> PaintBubble {
        return PaintBubble(level: level, position: position)
    }
    
    class func spawnBombBubble(level:Int, position:CGPoint) -> BombBubble {
        return BombBubble(level: level, position: position)
    }
}