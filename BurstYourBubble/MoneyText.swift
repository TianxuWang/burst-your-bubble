//
//  MoneyLabel.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/21/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class MoneyText:SKLabelNode {
    
    var curMoney:Int!
    
    init(position: CGPoint) {
        super.init(fontNamed: FontName_Money)
        
        self.curMoney = money!
        self.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        self.position = position
        self.fontSize = 20
        self.fontColor = Color_Gold
        self.text = "• \(curMoney)"
    }
    
    func update() {
        if (curMoney < money!) {
            var array = NSMutableArray()
            while (curMoney != money!) {
                curMoney = curMoney + 1
                var moneyStr = "• \(curMoney)"
                var action = SKAction.sequence([
                    SKAction.runBlock({
                        self.text = moneyStr
                        if (userSettings.sfxEnabled) {
                            self.runAction(digitSfxAction)
                        }
                    }),
                    SKAction.waitForDuration(0.01)
                ])
                array.addObject(action)
            }
                
            self.runAction(SKAction.sequence(array as [AnyObject]))
        }
        else if (curMoney > money) {
            curMoney = money!
            self.text = "• \(curMoney)"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(fontNamed fontName: String!) {
        super.init(fontNamed: fontName)
    }
    
    override init() {
        super.init()
    }
}
