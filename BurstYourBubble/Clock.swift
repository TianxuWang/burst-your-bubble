//
//  Clock.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/5/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class Clock: SKSpriteNode {
   
    var bonusTimeLabel:SKLabelNode!
    var multiplierLabel:SKLabelNode!
    var time:Int = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor?, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    init(time: Int, multiplier:Int, initPosition: CGPoint, destPosition: CGPoint) {
        super.init(texture: clockTexture, color: UIColor.clearColor(), size: clockTexture.size())
        
        self.name = "clock"
        self.userInteractionEnabled = false
        self.position = initPosition
        self.zPosition = -1
        self.time = time * multiplier
        
        bonusTimeLabel = SKLabelNode(fontNamed: FontName_Symbol)
        bonusTimeLabel.name = "bonusTimeLabel"
        bonusTimeLabel.text = String(time)
        bonusTimeLabel.fontColor = Color_Green
        bonusTimeLabel.fontSize = 75
        bonusTimeLabel.position = CGPointMake(-2, -26)
        bonusTimeLabel.userInteractionEnabled = false
        self.addChild(bonusTimeLabel)
        self.zPosition = -1
        
        if (multiplier > 1) {
            multiplierLabel = SKLabelNode(fontNamed: FontName_Symbol)
            multiplierLabel.name = "multiplierLabel"
            multiplierLabel.text = "x\(multiplier)"
            multiplierLabel.fontColor = Color_Green
            multiplierLabel.fontSize = 75
            multiplierLabel.position = CGPointMake(-2, -180)
            multiplierLabel.userInteractionEnabled = false
            self.addChild(multiplierLabel)
            self.zPosition = -1
        }
        
        // init actions
        var vector = CGVectorMake(destPosition.x - position.x, destPosition.y - position.y)
        self.alpha = 0
        if (userSettings.sfxEnabled) {
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(0.2),
                clockInitSfxAction,
                SKAction.waitForDuration(1.5),
                clockWindUpSfxAction
            ]))
        }

        if (multiplier > 1) {
            var shakeAmplitude = (multiplier - 2) * 7 + 7
            self.runAction(SKAction.sequence([
                SKAction.waitForDuration(0.2),
                SKAction.runBlock({
                    Utility.shake(self, amplitude: UInt32(shakeAmplitude), duration: 0.2, frequency: 40)
                })
            ]))
        }
        
        self.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.fadeAlphaTo(0.8, duration: 0.2),
                SKAction.scaleTo(0.2, duration: 0.2)
            ]),
            SKAction.waitForDuration(1.0),
            SKAction.group([
                SKAction.scaleTo(0.1, duration: 0.6),
                SKAction.moveBy(vector, duration: 0.6),
                SKAction.fadeAlphaTo(0.2, duration: 0.6)
            ])
            ]),
            completion: {
                var scene:GameEndlessModeScene = self.scene! as! GameEndlessModeScene
                scene.addTimeLeft(self.time)
                self.removeFromParent()
            }
        )
    }
}
