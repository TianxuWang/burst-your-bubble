//
//  GameCenterHelper.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 11/16/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import Foundation
import SpriteKit
import GameKit

let AchievementID_ST100:String = "ST100"
let AchievementID_ST500:String = "ST500"
let AchievementID_ST1000:String = "ST1000"
let AchievementID_ST1500:String = "ST1500"
let AchievementID_ST2000:String = "ST2000"
let AchievementID_ST3000:String = "ST3000"

let AchievementID_TB100:String = "TB100"
let AchievementID_TB500:String = "TB500"
let AchievementID_TB1000:String = "TB1000"
let AchievementID_TB5000:String = "TB5000"
let AchievementID_TB10000:String = "TB10000"

let AchievementID_TPB10:String = "TPB10"
let AchievementID_TPB100:String = "TPB100"
let AchievementID_TPB500:String = "TPB500"
let AchievementID_TPB1000:String = "TPB1000"

let AchievementID_TBB10:String = "TBB10"
let AchievementID_TBB100:String = "TBB100"
let AchievementID_TBB500:String = "TBB500"
let AchievementID_TBB1000:String = "TBB1000"

class GameCenterHelper {
    
    class func getAchievementByID(identifier: String) -> GKAchievement {
        var achievement:GKAchievement? = achievementMap.objectForKey(identifier) as! GKAchievement?
        if (achievement == nil) {
            achievement = GKAchievement(identifier: identifier)
            achievementMap.setObject(achievement!, forKey: identifier)
        }
        return achievement!
    }
    
    class func loadAchievements() {
        achievementMap = NSMutableDictionary()
        GKAchievement.loadAchievementsWithCompletionHandler( {(achievements: Array!, error: NSError!) -> Void in
            if (error == nil && achievements != nil) {
                for obj in achievements {
                    var achievement:GKAchievement = obj as! GKAchievement
                    achievementMap.setObject(achievement, forKey: achievement.identifier)
                }
            }
        })
    }
    
    class func reportAchievement(identifier: String) {
        var achievement:GKAchievement? = achievementMap.objectForKey(identifier) as! GKAchievement?
        if (achievement == nil) {
            achievement = GKAchievement(identifier: identifier)
            achievement!.percentComplete = 100.0
            achievement!.showsCompletionBanner = true
            
            achievementMap.setObject(achievement!, forKey: identifier)
            GKAchievement.reportAchievements([achievement!], withCompletionHandler: {(error: NSError!) -> Void in
                if (error == nil && achievement != nil && userSettings.sfxEnabled && !achievementSfxPlayer.playing) {
                    achievementSfxPlayer.play()
                }
            })
        }
    }
    
    class func resetAchievements() {
        achievementMap = NSMutableDictionary()
        GKAchievement.resetAchievementsWithCompletionHandler({(error: NSError!) -> Void in
            if (error != nil) {
                println(error.localizedDescription)
            }
        })
    }
}
