//
//  Utility.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/24/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import Foundation
import SpriteKit

class Utility {
    
    class func loadFramesWithName(baseFileName: String, numberOfFrames: Int) -> [SKTexture] {
        return [SKTexture](map(1...numberOfFrames) { i in
            let fileName = "\(baseFileName)_\(i)"
            return SKTexture(imageNamed: fileName)
        })
    }
    
    class func loadSoundActionsWithName(baseFileName: String, numberOfFiles: Int) -> [SKAction] {
        return [SKAction](map(1...numberOfFiles) { i in
            let fileName = "\(baseFileName)_\(i).wav"
            return SKAction.playSoundFileNamed(fileName, waitForCompletion: false)
        })
    }
    
    class func displayLabelByChar(label: SKLabelNode, text: NSString) {
        
        let charNum = text.length
        
        if (charNum > 0) {
            var actions:NSMutableArray = NSMutableArray()
            
            for var i = 0; i <= charNum; i++ {
                var subStr = text.substringToIndex(i)
                var action:SKAction = SKAction.sequence([
                    SKAction.runBlock({
                        label.text = subStr
                    }),
                    SKAction.waitForDuration(0.01)
                ])
                actions.addObject(action)
            }
            
            label.runAction(SKAction.sequence(actions as [AnyObject]), withKey: "displayLine")
        }
    }
    
    class func buttonLabelZoomIn(labelNode: SKLabelNode) {
        labelNode.runAction(SKAction.sequence([
            SKAction.runBlock({
                labelNode.alpha = 0
                labelNode.setScale(0.05)
            }),
            SKAction.waitForDuration(0.5),
            SKAction.group([
                SKAction.fadeInWithDuration(0.1),
                SKAction.sequence([
                    SKAction.scaleTo(1.2, duration: 0.2),
                    SKAction.scaleTo(1.0, duration: 0.02)
                ])
            ])
        ]))
    }
    
    class func screenFlash(scene: SKScene, times: Int, interval: NSTimeInterval) {
        scene.runAction(SKAction.repeatAction(
            SKAction.sequence([
                SKAction.waitForDuration(interval),
                SKAction.runBlock({
                    scene.backgroundColor = userSettings.bgColorBlack ? Color_White : Color_Black
                }),
                SKAction.waitForDuration(interval),
                SKAction.runBlock({
                    scene.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
                })])
            , count: times)
        )
    }

    class func initUnlockArray(length: Int) -> [Bool] {
        var arr = Array(count: length, repeatedValue: false)
        arr[0] = true
        
        return arr
    }
    
    class func shake(node:SKNode, amplitude:UInt32, duration:Double, frequency:Int) {
        let initialPos:CGPoint = node.position
        var randomActions:NSMutableArray = NSMutableArray()
        let times:Int = Int(duration * Double(frequency))
        for var i = 0; i < times; i++ {
            var randX:CGFloat = CGFloat(arc4random_uniform(amplitude)) - CGFloat(amplitude/2) + initialPos.x
            var randY:CGFloat = CGFloat(arc4random_uniform(amplitude)) - CGFloat(amplitude/2) + initialPos.y
            randomActions.addObject(SKAction.moveTo(CGPointMake(randX, randY), duration: Double(1/frequency)))
        }
        
        node.runAction(SKAction.sequence(randomActions as [AnyObject]), completion: {
            node.position = initialPos
        })
    }
}

extension String {
    subscript (r: Range<Int>) -> String {
        get {
            let startIndex = advance(self.startIndex, r.startIndex)
            let endIndex = advance(startIndex, r.endIndex - r.startIndex)
            
            return self[Range(start: startIndex, end: endIndex)]
        }
    }
}