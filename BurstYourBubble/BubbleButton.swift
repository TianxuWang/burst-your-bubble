//
//  BubbleButton.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/17/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

// Not used in current version
class BubbleButton {
    
    var bubbleSprite:ColorBubble!
    var textLabel:SKLabelNode?
    
    init(level: Int, text: String?, position:CGPoint) {
        bubbleSprite = BubbleFactory.spawnColorBubbleWithRandomColor(level, position: position)
        if (text != nil) {
            textLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        }
    }
    
}
