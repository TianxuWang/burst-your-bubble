//
//  ProgressBar.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 10/1/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class ProgressBar: SKCropNode {
    
    var frontImg:SKSpriteNode!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(imageName: String, position: CGPoint) {
        super.init()
        
        frontImg = SKSpriteNode(imageNamed: imageName)
        frontImg.position = position
        self.addChild(frontImg)
        
        self.maskNode = SKSpriteNode(color: Color_White, size: CGSizeMake(200, 10))
        self.maskNode!.position = position
        self.maskNode!.xScale = 0
    }
    
    func setProgress(fraction: CGFloat) {
        var curProgress = self.maskNode!.xScale
        var array:NSMutableArray = NSMutableArray()
        var times:Int = Int(CGFloat(fraction - curProgress) / CGFloat(0.05))
        for var i = 0; i < times; i++ {
            var action = SKAction.sequence([
                SKAction.runBlock({
                    self.maskNode!.xScale += 0.05
                }),
                SKAction.waitForDuration(0.01)
            ])
            array.addObject(action)
        }
        self.runAction(SKAction.sequence(array as [AnyObject]))
    }
}
