//
//  RegularBubble.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/15/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class ColorBubble: Bubble {
    
    var colorCategory:BubbleColor!
    var basicScore:Int = 1
    var sprites_Normal:[SKTexture]!
    var sprites_Scale:[SKTexture]!
    
    convenience init(color:BubbleColor, level:Int, position:CGPoint) {
        self.init(level: level, position: position)
        
        self.setColor(color)
    }
    
    override init(level:Int, position:CGPoint) {
        super.init(level: level, position: position)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func initializeSprites() {
        if (self.colorCategory == nil) {
            self.colorCategory = BubbleColor.randomColor()
        }
        
        self.spriteArray = Bubble.getAnimationFramesForColorBubble(colorCategory)
        
        self.sprites_Normal = Array(spriteArray[0..<4])
        
        self.sprites_Scale = Array(spriteArray[4..<ColorBubbleAnimationFrameNum])
        self.sprites_Scale = sprites_Scale + sprites_Scale.reverse()
        self.sprites_Scale.append(sprites_Normal[0])
        
        self.texture = self.spriteArray[0]
    }
    
    override func initializeAttributes() {
        self.size = self.texture!.size()
        self.basicScore = level == MaxLevel ? 500 : level + 1
        super.initializeAttributes()
    }
    
    override func initializePhysics() {
        super.initializePhysics()
        
        self.physicsBody!.collisionBitMask =
            ColorBubbleCategory | MovingBubbleCategory
    }
    
    override func initializeActions() {
        super.initializeActions()
        
        self.blowUpAnimAction = SKAction.sequence([
            SKAction.runBlock({
                self.runAction(SKAction.group([
                    SKAction.scaleTo(self.getScaleByLevel(self.level), duration: 0.7),
                    SKAction.animateWithTextures(self.sprites_Scale, timePerFrame: 0.07)
                ]))
            }),
            SKAction.waitForDuration(0.7),
            SKAction.runBlock({
                var velocity = self.physicsBody!.velocity
                self.initializePhysics()
                self.physicsBody!.categoryBitMask = ColorBubbleCategory
                self.physicsBody!.contactTestBitMask = PaintBubbleCategory | BombBubbleCategory
                // Slightly decrease the speed as the size has been increased.
                self.physicsBody!.velocity = CGVectorMake(velocity.dx * 0.8, velocity.dy * 0.8)
                
                var scene = self.scene! as! GameScene
                if (scene.gameStarted) {
                    self.userInteractionEnabled = true
                }
            })
        ])
        
        self.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.fadeAlphaTo(0.8, duration: 0.05),
                SKAction.scaleTo(1 - 0.1 * CGFloat(level), duration: 0.2)
            ]),
            SKAction.runBlock({
                self.physicsBody!.categoryBitMask = ColorBubbleCategory
                self.physicsBody!.contactTestBitMask = PaintBubbleCategory | BombBubbleCategory
            })
        ]))
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        
        var scene:GameScene = self.scene as! GameScene
        scene.scoreCalculator.addScore(self, updatePreColor: true)
        
        if (self.level < MaxLevel) {
            self.split()
        }
    }
    
    override func pop() {
        self.removeActionForKey("levelDown")
        
        self.alpha = 1.0
        self.zPosition = -1
        self.isAlive = false
        self.physicsBody!.categoryBitMask = 0x0
        self.physicsBody!.contactTestBitMask = 0x0
        self.physicsBody!.collisionBitMask = 0x0
        self.userInteractionEnabled = false
        
        // Don't change property 'dynamic' at runtime:
        // if by happens the other contacted body is also static (no-dynamic), and the collission/contact callback
        // is happening, it will throw assertion error - contact detected between 2 static bodies mid-execution.
        // Here is the work around:
        var temp:SKPhysicsBody = self.physicsBody!
        temp.dynamic = false
        self.physicsBody = temp
        
        if (userSettings.sfxEnabled && self.isAbleToPlaySfx()) {
            self.runAction(bubblePopSfxAction)
        }
        
        self.runAction(SKAction.sequence([
            SKAction.group([
                SKAction.animateWithTextures(sprites_Normal, timePerFrame: 0.07),
                SKAction.scaleBy(1.5, duration: 0.2)
            ]),
            SKAction.waitForDuration(2.0),
            SKAction.fadeOutWithDuration(0.2)
            ]),
            completion: {
                self.removeFromParent()
            }
        )
        
        if (self.scene! is GameScene && gameCenterEnabled) {
            // Check Total Bubble achievement
            userSettings.achievementsTrack[0]++
            switch(userSettings.achievementsTrack[0]) {
            case 100..<500:
                GameCenterHelper.reportAchievement(AchievementID_TB100)
            case 500..<1000:
                GameCenterHelper.reportAchievement(AchievementID_TB500)
            case 1000..<5000:
                GameCenterHelper.reportAchievement(AchievementID_TB1000)
            case 5000..<10000:
                GameCenterHelper.reportAchievement(AchievementID_TB5000)
            default:
                if (userSettings.achievementsTrack[0] >= 10000) {
                    GameCenterHelper.reportAchievement(AchievementID_TB10000)
                }
            }
        }
        
        super.pop()
    }
    
    override func decreaseOneLevel() {
        if (self.level > 0) {
            // Remove interactions
            self.userInteractionEnabled = false
            self.physicsBody!.categoryBitMask = 0x0
            self.physicsBody!.contactTestBitMask = 0x0
            
            self.level--
            self.basicScore = level + 1
            
            self.runAction(self.blowUpAnimAction, withKey: "levelDown")
        }
    }
    
    func split() {
        if (self.scene! is GameScene) {
            var scene:GameScene = self.scene! as! GameScene
            let newLevel:Int = self.level == MaxLevel ? self.level : self.level + 1
            
            var bubble1 = scene.bubbleCount <= 8 || CGFloat(arc4random_uniform(100)) + 1 > getRandomLikelyByLevel() ?
                          BubbleFactory.spawnColorBubbleWithColor(self.colorCategory, level: newLevel, position: self.position) :
                          BubbleFactory.spawnRandomBubble(newLevel, position: self.position)
            
            var bubble2 = scene.bubbleCount <= 8 || self.level == 0 ?
                          BubbleFactory.spawnColorBubbleWithColor(self.colorCategory, level: newLevel, position: self.position) :
                          BubbleFactory.spawnRandomBubble(newLevel, position: self.position)
            
            self.parent!.addChild(bubble1)
            self.parent!.addChild(bubble2)
            
            var flipX:Bool = CGFloat(arc4random_uniform(2)) > 0
            var flipY:Bool = CGFloat(arc4random_uniform(2)) > 0
            
            bubble1.applyRandomImpulseOnDirection(flipX, directionY: flipY)
            bubble2.applyRandomImpulseOnDirection(!flipX, directionY: !flipY)
        
            scene.bubblePool.append(bubble1)
            scene.bubblePool.append(bubble2)
        }
    }
    
    func setColor(color:BubbleColor) {
        self.colorCategory = color
        self.initializeSprites()
    }
    
    func shuffleColor() {
        self.setColor(BubbleColor.randomColor())
    }
    
    func getRandomLikelyByLevel() -> CGFloat {
        return CGFloat(self.level * 10)
    }
}
