//
//  GameScene.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/27/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import AVFoundation
import SpriteKit

// Make the time slightly greater than 60 to take moving time into consideration
let InitialTime_Time:Int = 60
let InitialTime_Endless:Int = 90
let InitialTime_Challenge:Int = 10
let InitialGoal_Challenge:Int = 100
let LeaderboardID_Time:String = "TMHS"
let LeaderboardID_Endless:String = "EMHS"
let LeaderboardID_Challenge:String = "CMHS"
let BoundaryThickness:CGFloat = 8

var colorBoundaryTextures:[SKTexture] = [SKTexture]()
var clockTexture:SKTexture!
var buttonSfxAction:SKAction!
var timerSfxAction:SKAction!
var clockWindUpSfxAction:SKAction!
var clockInitSfxAction:SKAction!
var drumWhirlingSfxPlayer:AVAudioPlayer!
var drumGoodSfxAction:SKAction!
var drumBadSfxAction:SKAction!
var updateSfxAction:SKAction!
var successSfxAction:SKAction!
var achievementSfxPlayer:AVAudioPlayer!
var gameMode:GameMode?

enum GameMode {
    case Time
    case Endless
    case Challenge
    
    func getLeaderboardID() -> String {
        switch(self) {
        case .Time:
            return LeaderboardID_Time
        case .Endless:
            return LeaderboardID_Endless
        case .Challenge:
            return LeaderboardID_Challenge
        }
    }
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var gameStarted:Bool = false
    var bubbleCount:Int = 0
    
    var gameLayer:SKSpriteNode!
    var pauseMenuLayer:SKSpriteNode!
    
    var pauseButton:SKSpriteNode!
    var colorBoundary:Boundary!
    
    var bubblePool:[Bubble] = [Bubble]()
    var scoreCalculator:ScoreCalculator!
    
    override init(size:CGSize) {
        super.init(size:size)
        
        initAttributes()
        initUIs()
        initBubbles()
        initMusicPlayer()
        
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                self.gameStart()
            })
        ]))
    }
    
    func initAttributes() {
        // Initialize scene
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        self.physicsWorld.contactDelegate = self
        
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: CGRectMake(
            self.frame.origin.x + BoundaryThickness, self.frame.origin.y + BoundaryThickness,
            self.frame.width - BoundaryThickness * 2, self.frame.height - BoundaryThickness * 2)
        )
        self.physicsBody!.friction = 0
        
        // Initialize score calculator
        scoreCalculator = ScoreCalculator()
        
        // Use 2 sprite nodes to serve as 2 "layer" in order to implement pausing
        gameLayer = SKSpriteNode(color: SKColor.clearColor(), size: self.frame.size)
        gameLayer.name = "gameLayer"
        self.addChild(gameLayer)
    }
    
    func initUIs() {
        // Boundary
        colorBoundary = Boundary(size: self.frame.size)
        gameLayer.addChild(colorBoundary)
        
        // Pause button
        pauseButton = SKSpriteNode(imageNamed: "Pause.png")
        pauseButton.name = "pause"
        pauseButton.setScale(0.3)
        pauseButton.zPosition = 5
        pauseButton.position = CGPointMake(20, 20)
        gameLayer.addChild(pauseButton)
        pauseButton.hidden = true
    }
    
    func initBubbles() {
        var bubble:ColorBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            0,                                                                          // bubble level (0~9): detemine size and speed
            position: CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)    // initial position of bubble
        )
        bubble.userInteractionEnabled = false
        gameLayer.addChild(bubble)
    }
    
    func initMusicPlayer() {
        bgMusicPlayer.pause()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func update(currentTime: CFTimeInterval) {
        if (!gameLayer.paused) {
            updateUIs(currentTime)
            if (gameStarted) {
                updateBubble()
                updateAchievement()
                updateGameOverCondition()
            }
        }
    }
    
    func updateBubble() {
        bubbleCount = 0
        let node = gameLayer.children
        for obj in node {
            if (obj.name == "bubble") {
                bubbleCount++
                var bubble:Bubble = obj as! Bubble
                bubble.update()
            }
        }
    }
    
    func updateAchievement() { }
    
    func updateUIs(currentTime: CFTimeInterval) {
        if (scoreCalculator.prevColor != nil) {
            colorBoundary.updateTexture(scoreCalculator.prevColor)
        }
    }
    
    func updateGameOverCondition() {
        if (bubbleCount == 0) {
            gameEnd()
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil) {
                if (node!.name == "pause") {
                    gamePause()
                }
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody:SKPhysicsBody
        var secondBody:SKPhysicsBody
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // Collission between BombBubble and OTHER bubbles
        if (firstBody.categoryBitMask == BombBubbleCategory || secondBody.categoryBitMask == BombBubbleCategory) {
            if (firstBody.node!.name == "bubble" && secondBody.node!.name == "bubble") {
                var bubble_1 = firstBody.node as! Bubble
                var bubble_2 = secondBody.node as! Bubble
                
                if (bubble_1.isAlive) {
                    if (bubble_1 is BombBubble && bubble_2 is BombBubble) {
                        var bombBubble_1:BombBubble = bubble_1 as! BombBubble
                        var bombBubble_2:BombBubble = bubble_2 as! BombBubble
                        bombBubble_1.bonusTimeMultiplier = bombBubble_2.bonusTimeMultiplier + 1
                    }
                    
                    bubble_1.pop()
                    
                    if (bubble_1 is ColorBubble) {
                        var colorBubble:ColorBubble = bubble_1 as! ColorBubble
                        scoreCalculator.addScore(colorBubble, updatePreColor: false)
                        if (scoreCalculator.prevColor != nil && colorBubble.colorCategory == scoreCalculator.prevColor) {
                            var bombBubble:BombBubble = bubble_2 as! BombBubble
                            //bombBubble.bonusTime += (MaxLevel - colorBubble.level + 1)
                            bombBubble.bonusTime += (scoreCalculator.bonusSfxIndex == BubblePopScaleSfxNum - 1 ? 2 : 1)
                        }
                    }
                }
                if (bubble_2.isAlive) {
                    if (bubble_2 is BombBubble && bubble_1 is BombBubble) {
                        var bombBubble_2:BombBubble = bubble_2 as! BombBubble
                        var bombBubble_1:BombBubble = bubble_1 as! BombBubble
                        bombBubble_2.bonusTimeMultiplier = bombBubble_1.bonusTimeMultiplier + 1
                    }
                    
                    bubble_2.pop()
                    
                    if (bubble_2 is ColorBubble) {
                        var colorBubble:ColorBubble = bubble_2 as! ColorBubble
                        scoreCalculator.addScore(colorBubble, updatePreColor: false)
                        if (scoreCalculator.prevColor != nil && colorBubble.colorCategory == scoreCalculator.prevColor) {
                            var bombBubble:BombBubble = bubble_1 as! BombBubble
                            //bombBubble.bonusTime += (MaxLevel - colorBubble.level + 1)
                            bombBubble.bonusTime += (scoreCalculator.bonusSfxIndex == BubblePopScaleSfxNum - 1 ? 2 : 1)
                        }
                    }
                }
            }
        }
        // Collision between ColorBubble and PaintBubble
        else if (firstBody.categoryBitMask == ColorBubbleCategory && secondBody.categoryBitMask == PaintBubbleCategory) {
            var colorBubble:ColorBubble = firstBody.node as! ColorBubble
            var paintBubble:PaintBubble = secondBody.node as! PaintBubble
            
            colorBubble.setColor(paintBubble.paintColor!)
            scoreCalculator.prevColor = paintBubble.paintColor!
        }
        
        // Collision between ColorBubbles
//        else if (firstBody.categoryBitMask == ColorBubbleCategory && secondBody.categoryBitMask == ColorBubbleCategory) {
//            var bubbleBody_1:SKPhysicsBody = contact.bodyA
//            var bubbleBody_2:SKPhysicsBody = contact.bodyB
//            var bubble_1:ColorBubble = bubbleBody_1.node as ColorBubble
//            var bubble_2:ColorBubble = bubbleBody_2.node as ColorBubble
//            
//            // Use Kinetic Energy formula to calculate implusion: 1/2 * m * v * v
//            // Here we assume all bubble have same mass (the bigger bubble will has less density)
//            // So we only calculate and compare v*v
//            var ke_1 = (bubbleBody_1.velocity.dx * bubbleBody_1.velocity.dx) + (bubbleBody_1.velocity.dy * bubbleBody_1.velocity.dy)
//            var ke_2 = (bubbleBody_2.velocity.dx * bubbleBody_2.velocity.dx) + (bubbleBody_2.velocity.dy * bubbleBody_2.velocity.dy)
//            
//            if (ke_1 > MaxExternalImpluseTolerance && bubble_2.isAlive) {
//                scoreCalculator.addScore(bubble_2, updatePreColor: false)
//                bubble_2.pop()
//            }
//            
//            if (ke_2 > MaxExternalImpluseTolerance && bubble_1.isAlive) {
//                scoreCalculator.addScore(bubble_1, updatePreColor: false)
//                bubble_1.pop()
//            }
//        }
    }
    
    func gameStart() {
        // hide ad banner
        NSNotificationCenter.defaultCenter().postNotificationName("hideAdBanner", object: nil)
        gameStarted = true

        // Give the first bubble a initial impluse
        for obj in gameLayer.children {
            if (obj.name == "bubble") {
                var bubble:Bubble = obj as! Bubble
                bubble.userInteractionEnabled = true
                bubble.applyRandomImpulse()
            }
        }
        
        pauseButton.hidden = false
        
        ViewController.playTrackAtIndex(userSettings.selectedMusicThemeId)
        bgMusicPlayer.play()
    }
    
    func gamePause() {
        NSNotificationCenter.defaultCenter().postNotificationName("showAdBanner", object: nil)
        pauseButton.hidden = true
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.1),
            SKAction.runBlock({
                self.gameLayer.paused = true
            })
        ]))
        self.physicsWorld.speed = 0.0
        for obj in gameLayer.children {
            if (obj is Bubble) {
                var bubble:Bubble = obj as! Bubble
                bubble.userInteractionEnabled = false
            }
        }
        
        if (pauseMenuLayer == nil) {
            pauseMenuLayer = PauseMenuLayer(size: self.frame.size)
            pauseMenuLayer.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)
            self.addChild(pauseMenuLayer)
        }
        
        bgMusicPlayer.pause()
    }
    
    func gameUnpause() {
        NSNotificationCenter.defaultCenter().postNotificationName("hideAdBanner", object: nil)
        
        pauseMenuLayer.removeFromParent()
        pauseMenuLayer = nil
        pauseButton.hidden = false
        
        gameLayer.paused = false
        if (gameStarted) {
            self.physicsWorld.speed = 1.0
            for obj in gameLayer.children {
                if (obj is Bubble) {
                    var bubble:Bubble = obj as! Bubble
                    bubble.userInteractionEnabled = true
                }
            }
        }
        
        bgMusicPlayer.play()
    }
    
    func gameEnd() {   
        gameStarted = false
        // Save score (if needed)
        var record:Int? = self.scoreCalculator.saveScore()
        record = record == nil ? 0 : record!
        
        // Save achievement track
        NSNotificationCenter.defaultCenter().postNotificationName("saveSettings", object: nil)
        
        // Switch to GameOverScene
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 1.5)
                var gameOverScene:SKScene = GameOverScene(size: self.size, score: self.scoreCalculator.totalScore, record: record!)
                self.view!.presentScene(gameOverScene, transition: transition)
                bgMusicPlayer.pause()
            })
        ]))
    }

    func gameRestart() {
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                var gameScene:SKScene = GameScene(size: self.size)
                self.view!.presentScene(gameScene, transition: transition)
            })
        ]))
    }
    
    class func loadSceneAssets() {
        // Preload all assets here
        clockTexture = SKTexture(imageNamed: "clock")
        
        // UI sfx
        timerSfxAction = SKAction.playSoundFileNamed("beep.wav", waitForCompletion: false)
        clockInitSfxAction = SKAction.playSoundFileNamed("clock_init.wav", waitForCompletion: false)
        clockWindUpSfxAction = SKAction.playSoundFileNamed("clock_windup.wav", waitForCompletion: false)
        
        var url_drum:NSURL = NSBundle.mainBundle().URLForResource("drum_whirling", withExtension: "wav")!
        drumWhirlingSfxPlayer = AVAudioPlayer(contentsOfURL: url_drum, error: nil)
        drumWhirlingSfxPlayer.numberOfLoops = -1
        drumWhirlingSfxPlayer.prepareToPlay()
        drumGoodSfxAction = SKAction.playSoundFileNamed("drum_good.wav", waitForCompletion: true)
        drumBadSfxAction = SKAction.playSoundFileNamed("drum_bad.wav", waitForCompletion: true)
        
        updateSfxAction = SKAction.playSoundFileNamed("upgrade.wav", waitForCompletion: false)
        successSfxAction = SKAction.playSoundFileNamed("success.wav", waitForCompletion: false)
        
        var url_achievement:NSURL = NSBundle.mainBundle().URLForResource("achievement", withExtension: "wav")!
        achievementSfxPlayer = AVAudioPlayer(contentsOfURL: url_achievement, error: nil)
        achievementSfxPlayer.prepareToPlay()
    }
}
