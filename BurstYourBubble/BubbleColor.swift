//
//  BubbleColor.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/14/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

enum BubbleColor: Int {
    case Salmon = 0
    case Cantaloupe
    case Banana
    case Honeydew
    case Flora
    case Spindrift
    case Ice
    case Sky
    case Orchid
    case Lavender
    case Carnation
    
    static var ColorPool:[BubbleColor] =
    [Salmon, Cantaloupe, Banana, Honeydew, Flora, Spindrift, Ice, Sky, Orchid, Lavender, Carnation]
    
    static func randomColor() -> BubbleColor {
        return BubbleColor.ColorPool[Int(arc4random_uniform(UInt32(curPalette.colorNum)))]
    }
    
    static func getColorByIndex(index:Int) -> BubbleColor {
        return ColorPool[index]
    }
    
    static func getNextColor(color: BubbleColor) -> BubbleColor {
        var index:Int? = find(self.ColorPool, color)
        var nextIndex = index! >= curPalette.colorNum - 1 ? 0 : index! + 1
        return ColorPool[nextIndex]
    }
    
    static func addColor(color:BubbleColor) {
        BubbleColor.ColorPool.append(color)
    }
    
    static func removeColor(color:BubbleColor) {
        for (index, element) in enumerate(BubbleColor.ColorPool) {
            if (element == color) {
                BubbleColor.ColorPool.removeAtIndex(index)
            }
        }
    }
    
    func colorDifferenceTo(color:BubbleColor) -> Int {
        return min(abs(self.rawValue - color.rawValue), curPalette.colorNum - abs(self.rawValue - color.rawValue))
    }
    
    func toString() -> String {
        switch self {
        case .Salmon:
            return "Salmon"
        case .Cantaloupe:
            return "Cantaloupe"
        case .Banana:
            return "Banana"
        case .Honeydew:
            return "Honeydew"
        case .Flora:
            return "Flora"
        case .Spindrift:
            return "Spindrift"
        case .Ice:
            return "Ice"
        case .Sky:
            return "Sky"
        case .Orchid:
            return "Orchid"
        case .Lavender:
            return "Lavender"
        case .Carnation:
            return "Carnation"
        default:
            return ""
        }
    }
    
    func toSKColor() -> SKColor {
        return curPalette.colors[self.rawValue]
    }
}
