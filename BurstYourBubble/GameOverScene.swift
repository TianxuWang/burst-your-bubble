//
//  GameOverScene.swift
//  BubbleBang
//
//  Created by Tianxu Wang on 8/30/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import UIKit
import SpriteKit

var fireworkSfxAction:SKAction!
var cheerSfxAction:SKAction!

class GameOverScene: SKScene {
    
    var restartBubble:ColorBubble!
    var gameOverLabel:SKLabelNode!
    var recordLabel:SKLabelNode!
    var scoreLabel:SKLabelNode!
    var restartLabel:SKLabelNode!
    var menuBubble:ColorBubble!
    var menuLabel:SKLabelNode!
    var moneyText:MoneyText!
    var emitter_left:BubbleEmitter!
    var emitter_right:BubbleEmitter!
    
    init(size: CGSize, score: Int, record: Int) {
        super.init(size: size)
        
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        
        gameOverLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        gameOverLabel.fontSize = 40
        gameOverLabel.fontColor = Color_Gray
        gameOverLabel.text = "Game Over"
        gameOverLabel.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + 90)
        self.addChild(gameOverLabel)
        
        // Money Text
        moneyText = MoneyText(position: CGPointMake(self.frame.size.width - 13, self.frame.size.height - 30))
        self.addChild(moneyText)
        
        recordLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        recordLabel.fontSize = 20
        recordLabel.fontColor = Color_Gray
        recordLabel.text = "Record: \(record)"
        recordLabel.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + 40)
        self.addChild(recordLabel)
        
        scoreLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        scoreLabel.fontSize = 20
        scoreLabel.fontColor = Color_Gray
        scoreLabel.text = "Your Score: \(score)"
        scoreLabel.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + 10)
        scoreLabel.alpha = 0
        scoreLabel.setScale(0.05)
        self.addChild(scoreLabel)
        scoreLabel.runAction(SKAction.sequence([
            SKAction.waitForDuration(1.0),
            SKAction.group([
                SKAction.scaleTo(1.0, duration: 0.2),
                SKAction.fadeInWithDuration(0.1)
            ]),
            SKAction.waitForDuration(0.2),
            SKAction.runBlock({
                // For new record
                if (score > record) {
                    self.recordLabel.runAction(SKAction.sequence([
                        SKAction.scaleTo(1.3, duration: 0.2),
                        SKAction.runBlock({
                            self.recordLabel.text = "New Record: \(score)"
                        }),
                        SKAction.waitForDuration(0.2),
                        SKAction.scaleTo(1.0, duration: 0.2),
                        SKAction.repeatActionForever(SKAction.sequence([
                            SKAction.scaleTo(1.2, duration: 0.5),
                            SKAction.scaleTo(1.0, duration: 0.5)
                        ]))
                    ]))
                    
                    self.emitter_left = BubbleEmitter(node: self, name: "leftEmitter", level: 8, pos: CGPointMake(0, 0), directionX: true, directionY: true, interval: 0.02, lastTime: 0.4, rangeTime: 0.15)
                    self.emitter_right = BubbleEmitter(node: self, name: "rightEmitter", level: 8, pos: CGPointMake(self.frame.size.width, 0), directionX: false, directionY: true, interval: 0.02, lastTime: 0.4, rangeTime: 0.15)
                    
                    self.runAction(SKAction.repeatAction(SKAction.sequence([
                        SKAction.runBlock({
                            if (userSettings.sfxEnabled) {
                                self.runAction(fireworkSfxAction)
                            }
                            
                            self.emitter_left.emitOnce()
                            self.emitter_right.emitOnce()
                            Utility.screenFlash(self, times: 1, interval: 0.1)
                        }),
                        SKAction.waitForDuration(0.6)
                    ]), count: 3))
                    
                    if (userSettings.sfxEnabled) {
                        self.runAction(cheerSfxAction)
                    }
                }
            }),
            SKAction.waitForDuration(score > record ? 2.8 : 0.5),
            SKAction.runBlock({
                var gainMoney = gameMode! == GameMode.Challenge ?
                    score * 20 + (score - 2) * 500 * (score - 1) / 200 :
                    score / 100
                // Save money
                money! += gainMoney
                NSUserDefaults.standardUserDefaults().setObject(money!, forKey: "money")
                NSUserDefaults.standardUserDefaults().synchronize()
                
                // Float text
                var floatText = FloatText(
                    text: "+\(gainMoney)",
                    position: CGPointMake(self.scoreLabel.position.x + 100, self.scoreLabel.position.y),
                    fontName: FontName_Money,
                    fontSize: 20,
                    fontColor: Color_Gold
                )
                self.addChild(floatText)
                if (userSettings.sfxEnabled) {
                    floatText.runAction(moneySfxAction)
                }
            }),
            SKAction.waitForDuration(0.5),
            SKAction.runBlock({
                // Restart button
                self.restartBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
                    2,                                                                               // bubble level (0~9): detemine size and speed
                    position: CGPointMake(self.frame.size.width/2 - 60, self.frame.size.height/2 - 75)    // initial position of bubble
                )
                self.restartBubble.name = "restartBubble"
                self.restartBubble.userInteractionEnabled = false
                self.addChild(self.restartBubble)
                
                self.restartLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
                self.restartLabel.name = "restartLabel"
                self.restartLabel.fontSize = 20
                self.restartLabel.text = "Restart"
                self.restartLabel.position = CGPointMake(self.frame.size.width/2 - 60, self.frame.size.height/2 - 82)
                self.restartLabel.userInteractionEnabled = false
                self.addChild(self.restartLabel)
                Utility.buttonLabelZoomIn(self.restartLabel)
                
                // Menu button
                self.menuBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
                    2,
                    position: CGPointMake(self.frame.size.width/2 + 60, self.frame.size.height/2 - 75)
                )
                self.menuBubble.name = "menuBubble"
                self.menuBubble.userInteractionEnabled = false
                self.addChild(self.menuBubble)
                
                self.menuLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
                self.menuLabel.name = "menuLabel"
                self.menuLabel.fontSize = 22
                self.menuLabel.text = "Menu"
                self.menuLabel.position = CGPointMake(self.frame.size.width/2 + 60, self.frame.size.height/2 - 82)
                self.menuLabel.userInteractionEnabled = false
                self.addChild(self.menuLabel)
                Utility.buttonLabelZoomIn(self.menuLabel)
            })
        ]))
        
        self.runAction(SKAction.sequence([
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                NSNotificationCenter.defaultCenter().postNotificationName("showAdBanner", object: nil)
                
                ViewController.playMenuBgMusic()
                bgMusicPlayer.play()
            })
        ]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func update(currentTime: NSTimeInterval) {
        super.update(currentTime)
        
        moneyText.update()
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if (restartBubble != nil && menuBubble != nil) {
            if let touch = touches.first as? UITouch {
                var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
                if (node != nil && node!.name != nil ) {
                    if (node!.name == "restartBubble" || node!.name == "restartLabel") {
                        if (userSettings.sfxEnabled) {
                            restartBubble.runAction(buttonSfxAction)
                        }
                        restartBubble.runAction(SKAction.sequence([
                            SKAction.runBlock({
                                Utility.shake(self.restartBubble, amplitude: 4, duration: 0.5, frequency: 20)
                            }),
                            SKAction.waitForDuration(0.5),
                            SKAction.runBlock({
                                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                                
                                var gameScene:SKScene
                                switch(gameMode!) {
                                case GameMode.Time:
                                    gameScene = GameTimeModeScene(size: self.size)
                                case GameMode.Endless:
                                    gameScene = GameEndlessModeScene(size: self.size)
                                case GameMode.Challenge:
                                    gameScene = GameChallengeModeScene(size: self.size)
                                }
                                self.view!.presentScene(gameScene, transition: transition)
                            })
                            ]))
                    }
                    else if (node!.name == "menuBubble" || node!.name == "menuLabel") {
                        if (userSettings.sfxEnabled) {
                            menuBubble.runAction(buttonSfxAction)
                        }
                        menuBubble.runAction(SKAction.sequence([
                            SKAction.runBlock({
                                Utility.shake(self.menuBubble, amplitude: 4, duration: 0.5, frequency: 20)
                            }),
                            SKAction.waitForDuration(0.5),
                            SKAction.runBlock({
                                var menuScene:GameMenuScene = GameMenuScene(size: self.size)
                                var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                                self.view!.presentScene(menuScene, transition: transition)
                            })
                            ]))
                    }
                }
            }
        }
    }
    
    class func loadSceneAssets() {
        
        fireworkSfxAction = SKAction.playSoundFileNamed("firework.wav", waitForCompletion: false)
        cheerSfxAction = SKAction.playSoundFileNamed("cheer.wav", waitForCompletion: false)
        
    }
}
