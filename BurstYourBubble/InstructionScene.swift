//
//  InstructionScene1.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 9/27/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import UIKit
import SpriteKit

let InstructionTexts:[[String]] = [
    [
        "Tap a colored bubble to gain points as well as",
        "make it split. Always try to tap on the same",
        "color to accumulate bonus points."
    ],
    [
        "Switching between colors will decrease the",
        "bonus. Tap a bubble closer in color to the",
        "current color to retain the most bonus points."
    ],
    [
        "Tap a paint bubble to paint nearby bubbles",
        "with a random color. It also changes the",
        "current color so you can keep bonus points!"
    ],
    [
        "Tap a bomb bubble to burst all nearby bubbles",
        "and get points instantly. But bombed bubbles",
        "won't split into new bubbles, so be wise."
    ]
]


class InstructionScene: SKScene, SKPhysicsContactDelegate {
    
    var DemoOriginPos:CGPoint!
    
    var demoActionIndex:Int = 0
    var demoActions:[SKAction] = [SKAction]()
    var demoLayer:SKSpriteNode!
    var scoreCalculator:ScoreCalculator!
    
    // UI
    var titleLabel:SKLabelNode!
    var instructionLabel1:SKLabelNode!
    var instructionLabel2:SKLabelNode!
    var instructionLabel3:SKLabelNode!
    var nextBubble:ColorBubble!
    var nextLabel:SKLabelNode!
    var prevBubble:ColorBubble!
    var prevLabel:SKLabelNode!
    var backBubble:ColorBubble!
    var backLabel:SKLabelNode!
    var pagerLabel:SKLabelNode!
    
    // Sample bubbles
    var colorBubble1:ColorBubble!
    var colorBubble2:ColorBubble!
    var colorBubble3:ColorBubble!
    var colorBubble4:ColorBubble!
    var paintBubble:PaintBubble!
    var bombBubble:BombBubble!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.backgroundColor = userSettings.bgColorBlack ? Color_Black : Color_White
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        self.physicsWorld.contactDelegate = self
        
        // Initialize UIs
        titleLabel = SKLabelNode(fontNamed: FontName_DefaultBold)
        titleLabel.name = "titleLabel"
        titleLabel.fontSize = 40
        titleLabel.fontColor = Color_Gray
        titleLabel.text = "How To Play"
        titleLabel.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height - 100)
        self.addChild(titleLabel)
        
        pagerLabel = SKLabelNode(fontNamed: FontName_DefaultLight)
        pagerLabel.name = "pagerLabel"
        pagerLabel.fontSize = 15
        pagerLabel.fontColor = Color_Gray
        pagerLabel.position = CGPointMake(self.frame.size.width / 2, 60)
        self.addChild(pagerLabel)
        
        backBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            2,
            position: CGPointMake(0, self.frame.size.height)
        )
        backBubble.userInteractionEnabled = false
        backBubble.name = "backBubble"
        
        backLabel = SKLabelNode(fontNamed: FontName_SymbolBold)
        backLabel.fontSize = 23
        backLabel.text = "x"
        backLabel.position = CGPointMake(16, self.frame.size.height - 23)
        backLabel.name = "backLabel"
        Utility.buttonLabelZoomIn(backLabel)
        
        nextBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(self.frame.size.width, 135)
        )
        nextBubble.userInteractionEnabled = false
        nextBubble.name = "nextBubble"
        
        nextLabel = SKLabelNode(fontNamed: FontName_SymbolLight)
        nextLabel.fontSize = 20
        nextLabel.text = ">"
        nextLabel.position = CGPointMake(self.frame.size.width - 10, 129)
        nextLabel.name = "nextLabel"
        Utility.buttonLabelZoomIn(nextLabel)
        
        prevBubble = BubbleFactory.spawnColorBubbleWithRandomColor(
            5,
            position: CGPointMake(0, 135)
        )
        prevBubble.userInteractionEnabled = false
        prevBubble.name = "prevBubble"
        
        prevLabel = SKLabelNode(fontNamed: FontName_SymbolLight)
        prevLabel.fontSize = 20
        prevLabel.text = "<"
        prevLabel.position = CGPointMake(10, 129)
        prevLabel.name = "prevLabel"
        Utility.buttonLabelZoomIn(prevLabel)
        
        self.addChild(nextBubble)
        self.addChild(nextLabel)
        self.addChild(prevBubble)
        self.addChild(prevLabel)
        self.addChild(backBubble)
        self.addChild(backLabel)
        
        instructionLabel1 = SKLabelNode(fontNamed: FontName_DefaultLight)
        instructionLabel1.position = CGPointMake(35, 150)
        instructionLabel1.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        instructionLabel1.fontSize = 15
        instructionLabel1.fontColor = Color_Gray
        
        instructionLabel2 = SKLabelNode(fontNamed: FontName_DefaultLight)
        instructionLabel2.position = CGPointMake(35, 130)
        instructionLabel2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        instructionLabel2.fontSize = 15
        instructionLabel2.fontColor = Color_Gray
        
        instructionLabel3 = SKLabelNode(fontNamed: FontName_DefaultLight)
        instructionLabel3.position = CGPointMake(35, 110)
        instructionLabel3.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        instructionLabel3.fontSize = 15
        instructionLabel3.fontColor = Color_Gray
        
        self.addChild(instructionLabel1)
        self.addChild(instructionLabel2)
        self.addChild(instructionLabel3)
        
        updateInstructionTexts()
        
        // Initialize demos
        demoLayer = SKSpriteNode(texture: nil, color: SKColor.clearColor(), size: self.size)
        demoLayer.name = "demoLayer"
        demoLayer.userInteractionEnabled = false
        demoLayer.zPosition = -1
        self.addChild(demoLayer)
        
        DemoOriginPos = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2 + 40)
        initializeDemoActions()
        playDemoAction()
    }
    
    func updateInstructionTexts() {
        if (instructionLabel1.actionForKey("displayLine") != nil) {
            instructionLabel1.removeActionForKey("displayLine")
        }
        instructionLabel1.text = ""
        
        if (instructionLabel2.actionForKey("displayLine") != nil) {
            instructionLabel2.removeActionForKey("displayLine")
        }
        instructionLabel2.text = ""
        
        if (instructionLabel3.actionForKey("displayLine") != nil) {
            instructionLabel3.removeActionForKey("displayLine")
        }
        instructionLabel3.text = ""
        
        self.runAction(SKAction.sequence([
            SKAction.runBlock({
                Utility.displayLabelByChar(self.instructionLabel1, text: InstructionTexts[self.demoActionIndex][0])
            }),
            SKAction.waitForDuration(1.5),
            SKAction.runBlock({
                Utility.displayLabelByChar(self.instructionLabel2, text: InstructionTexts[self.demoActionIndex][1])
            }),
            SKAction.waitForDuration(1.5),
            SKAction.runBlock({
                Utility.displayLabelByChar(self.instructionLabel3, text: InstructionTexts[self.demoActionIndex][2])
            })
            ]),
            withKey: "display"
        )
    }
    
    func playDemoAction() {
        if (self.actionForKey("demo") == nil) {
            self.runAction(demoActions[demoActionIndex], withKey: "demo")
        }
    }
    
    func stopDemoAction() {
        if (self.actionForKey("demo") != nil) {
            self.removeActionForKey("demo")
        }
        
        demoLayer.removeAllChildren()
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        if let touch = touches.first as? UITouch {
            var node:SKNode? = nodeAtPoint(touch.locationInNode(self))
            if (node != nil && node!.name != nil) {
                if (node!.name == "nextBubble" || node!.name == "nextLabel") {
                    if (userSettings.sfxEnabled) {
                        self.nextBubble.runAction(buttonSfxAction)
                    }
                    self.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.nextBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            if (self.demoActionIndex < self.demoActions.count - 1) {
                                self.stopDemoAction()
                                self.demoActionIndex++
                                self.playDemoAction()
                                self.updateInstructionTexts()
                            }
                        })
                        ]))
                }
                else if (node!.name == "prevBubble" || node!.name == "prevLabel") {
                    if (userSettings.sfxEnabled) {
                        self.prevBubble.runAction(buttonSfxAction)
                    }
                    self.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.prevBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            if (self.demoActionIndex > 0) {
                                self.stopDemoAction()
                                self.demoActionIndex--
                                self.playDemoAction()
                                self.updateInstructionTexts()
                            }
                        })
                        ]))
                }
                else if (node!.name == "backBubble" || node!.name == "backLabel") {
                    if (userSettings.sfxEnabled) {
                        self.backBubble.runAction(buttonSfxAction)
                    }
                    self.runAction(SKAction.sequence([
                        SKAction.runBlock({
                            Utility.shake(self.backBubble, amplitude: 4, duration: 0.5, frequency: 20)
                        }),
                        SKAction.waitForDuration(0.5),
                        SKAction.runBlock({
                            var menuScene:GameMenuScene = GameMenuScene(size: self.size)
                            var transition:SKTransition = SKTransition.fadeWithColor(self.backgroundColor, duration: 0.5)
                            self.view!.presentScene(menuScene, transition: transition)
                        })
                        ]))
                }
            }
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        if (demoActionIndex == 0) {
            prevBubble.hidden = true
            prevLabel.hidden = true
        }
        else if (demoActionIndex == demoActions.count - 1) {
            nextBubble.hidden = true
            nextLabel.hidden = true
        }
        else {
            prevBubble.hidden = false
            prevLabel.hidden = false
            nextBubble.hidden = false
            nextLabel.hidden = false
        }
        
        pagerLabel.text = "\(demoActionIndex + 1) of \(demoActions.count)"
    }
    
    func initializeDemoActions() {
        var action1:SKAction = SKAction.repeatActionForever(SKAction.sequence([
            SKAction.runBlock({
                self.scoreCalculator = ScoreCalculator()
                
                self.colorBubble1 = BubbleFactory.spawnColorBubbleWithRandomColor(0, position: self.DemoOriginPos)
                self.colorBubble1.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble1)
            }),
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                self.colorBubble1.pop()
                self.scoreCalculator.addScore(self.colorBubble1, updatePreColor: true)
                
                var sampleBubble2 = BubbleFactory.spawnColorBubbleWithColor(
                    self.colorBubble1.colorCategory,
                    level: 1,
                    position: self.colorBubble1.position
                )
                var sampleBubble3 = BubbleFactory.spawnColorBubbleWithRandomColor(1, position: self.colorBubble1.position)
                sampleBubble2.userInteractionEnabled = false
                sampleBubble3.userInteractionEnabled = false
                self.demoLayer.addChild(sampleBubble2)
                self.demoLayer.addChild(sampleBubble3)
                
                sampleBubble2.runAction(SKAction.sequence([
                    SKAction.moveByX(-30, y: 0, duration: 0.5),
                    SKAction.waitForDuration(1.0),
                    SKAction.runBlock({
                        sampleBubble2.pop()
                        self.scoreCalculator.addScore(sampleBubble2, updatePreColor: true)
                        
                        var sampleBubble4 = BubbleFactory.spawnColorBubbleWithRandomColor(2, position: sampleBubble2.position)
                        var sampleBubble5 = BubbleFactory.spawnColorBubbleWithRandomColor(2, position: sampleBubble2.position)
                        sampleBubble4.userInteractionEnabled = false
                        sampleBubble5.userInteractionEnabled = false
                        self.demoLayer.addChild(sampleBubble4)
                        self.demoLayer.addChild(sampleBubble5)
                        
                        sampleBubble4.runAction(SKAction.sequence([
                            SKAction.moveBy(CGVectorMake(0, 40), duration: 0.5),
                            SKAction.waitForDuration(1.0, withRange: 0.2),
                            SKAction.runBlock({
                                sampleBubble4.pop()
                            })
                        ]))
                        sampleBubble5.runAction(SKAction.sequence([
                            SKAction.moveBy(CGVectorMake(0, -40), duration: 0.5),
                            SKAction.waitForDuration(1.0, withRange: 0.2),
                            SKAction.runBlock({
                                sampleBubble5.pop()
                            })
                        ]))
                    })
                ]))
                
                sampleBubble3.runAction(SKAction.sequence([
                    SKAction.moveByX(30, y: 0, duration: 0.5),
                    SKAction.waitForDuration(1.5),
                    SKAction.runBlock({
                        sampleBubble3.pop()
                        self.scoreCalculator.addScore(sampleBubble3, updatePreColor: true)
                        
                        var sampleBubble6 = BubbleFactory.spawnColorBubbleWithRandomColor(2, position: sampleBubble3.position)
                        var sampleBubble7 = BubbleFactory.spawnColorBubbleWithRandomColor(2, position: sampleBubble3.position)
                        sampleBubble6.userInteractionEnabled = false
                        sampleBubble7.userInteractionEnabled = false
                        self.demoLayer.addChild(sampleBubble6)
                        self.demoLayer.addChild(sampleBubble7)
                        
                        sampleBubble6.runAction(SKAction.sequence([
                            SKAction.moveBy(CGVectorMake(0, 40), duration: 0.5),
                            SKAction.waitForDuration(1.0, withRange: 0.2),
                            SKAction.runBlock({
                                sampleBubble6.pop()
                            })
                        ]))
                        sampleBubble7.runAction(SKAction.sequence([
                            SKAction.moveBy(CGVectorMake(0, -40), duration: 0.5),
                            SKAction.waitForDuration(1.0, withRange: 0.2),
                            SKAction.runBlock({
                                sampleBubble7.pop()
                            })
                        ]))
                    })
                ]))
            }),
            SKAction.waitForDuration(4.5)
        ]))
        
        var action2:SKAction = SKAction.repeatActionForever(SKAction.sequence([
            SKAction.runBlock({
                self.colorBubble1 = BubbleFactory.spawnColorBubbleWithRandomColor(5, position: CGPointMake(self.DemoOriginPos.x - 80, self.DemoOriginPos.y))
                self.colorBubble1.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble1)
                
                self.colorBubble2 = BubbleFactory.spawnColorBubbleWithColor(self.colorBubble1.colorCategory, level: 7, position: CGPointMake(self.DemoOriginPos.x - 20, self.DemoOriginPos.y + 40))
                self.colorBubble2.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble2)
                
                self.colorBubble3 = BubbleFactory.spawnColorBubbleWithColor(self.colorBubble1.colorCategory, level: 6, position: CGPointMake(self.DemoOriginPos.x + 20, self.DemoOriginPos.y - 40))
                self.colorBubble3.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble3)
                
                self.colorBubble4 = BubbleFactory.spawnColorBubbleWithRandomColor(5, position: CGPointMake(self.DemoOriginPos.x + 80, self.DemoOriginPos.y))
                self.colorBubble4.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble4)
                
                self.scoreCalculator = ScoreCalculator()
            }),
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                self.colorBubble1.runAction(SKAction.sequence([
                    SKAction.waitForDuration(0.5, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble1.pop()
                        self.scoreCalculator.addScore(self.colorBubble1, updatePreColor: true)
                    })
                ]))
                
                self.colorBubble2.runAction(SKAction.sequence([
                    SKAction.waitForDuration(0.8, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble2.pop()
                        self.scoreCalculator.addScore(self.colorBubble2, updatePreColor: true)
                    })
                ]))
                
                self.colorBubble3.runAction(SKAction.sequence([
                    SKAction.waitForDuration(1.1, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble3.pop()
                        self.scoreCalculator.addScore(self.colorBubble3, updatePreColor: true)
                    })
                ]))
                
                self.colorBubble4.runAction(SKAction.sequence([
                    SKAction.waitForDuration(1.4, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble4.pop()
                        self.scoreCalculator.addScore(self.colorBubble4, updatePreColor: true)
                    })
                ]))
            }),
            SKAction.waitForDuration(3.0)
        ]))
        
        var action3:SKAction = SKAction.repeatActionForever(SKAction.sequence([
            SKAction.runBlock({
                self.paintBubble = BubbleFactory.spawnPaintBubble(4, position: self.DemoOriginPos)
                self.paintBubble.userInteractionEnabled = false
                self.demoLayer.addChild(self.paintBubble)
                
                self.colorBubble1 = BubbleFactory.spawnColorBubbleWithRandomColor(3, position: CGPointMake(self.DemoOriginPos.x - 70, self.DemoOriginPos.y))
                self.colorBubble1.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble1)
                
                self.colorBubble2 = BubbleFactory.spawnColorBubbleWithRandomColor(5, position: CGPointMake(self.DemoOriginPos.x + 60, self.DemoOriginPos.y - 60))
                self.colorBubble2.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble2)
                
                self.colorBubble3 = BubbleFactory.spawnColorBubbleWithRandomColor(7, position: CGPointMake(self.DemoOriginPos.x + 70, self.DemoOriginPos.y + 70))
                self.colorBubble3.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble3)
                
                self.colorBubble4 = BubbleFactory.spawnColorBubbleWithRandomColor(4, position: CGPointMake(self.DemoOriginPos.x - 50, self.DemoOriginPos.y - 50))
                self.colorBubble4.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble4)
                
                self.scoreCalculator = ScoreCalculator()
            }),
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                self.paintBubble.pop()
                
                self.scoreCalculator = ScoreCalculator()
                self.colorBubble1.runAction(SKAction.sequence([
                    SKAction.waitForDuration(1.0, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble1.pop()
                        self.scoreCalculator.addScore(self.colorBubble1, updatePreColor: true)
                    })
                ]))
                
                self.colorBubble2.runAction(SKAction.sequence([
                    SKAction.waitForDuration(1.1, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble2.pop()
                        self.scoreCalculator.addScore(self.colorBubble2, updatePreColor: true)
                    })
                ]))
                
                self.colorBubble3.runAction(SKAction.sequence([
                    SKAction.waitForDuration(1.2, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble3.pop()
                        self.scoreCalculator.addScore(self.colorBubble3, updatePreColor: true)
                    })
                ]))
                
                self.colorBubble4.runAction(SKAction.sequence([
                    SKAction.waitForDuration(1.3, withRange: 0.2),
                    SKAction.runBlock({
                        self.colorBubble4.pop()
                        self.scoreCalculator.addScore(self.colorBubble4, updatePreColor: true)
                    })
                ]))
            }),
            SKAction.waitForDuration(2.0)
        ]))
        
        var action4:SKAction = SKAction.repeatActionForever(SKAction.sequence([
            SKAction.runBlock({
                self.bombBubble = BubbleFactory.spawnBombBubble(4, position: self.DemoOriginPos)
                self.bombBubble.userInteractionEnabled = false
                self.demoLayer.addChild(self.bombBubble)
                
                self.colorBubble1 = BubbleFactory.spawnColorBubbleWithRandomColor(3, position: CGPointMake(self.DemoOriginPos.x - 70, self.DemoOriginPos.y))
                self.colorBubble1.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble1)
                
                self.colorBubble2 = BubbleFactory.spawnColorBubbleWithRandomColor(5, position: CGPointMake(self.DemoOriginPos.x + 60, self.DemoOriginPos.y - 60))
                self.colorBubble2.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble2)
                
                self.colorBubble3 = BubbleFactory.spawnColorBubbleWithRandomColor(7, position: CGPointMake(self.DemoOriginPos.x + 70, self.DemoOriginPos.y + 70))
                self.colorBubble3.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble3)
                
                self.colorBubble4 = BubbleFactory.spawnColorBubbleWithRandomColor(4, position: CGPointMake(self.DemoOriginPos.x - 50, self.DemoOriginPos.y - 50))
                self.colorBubble4.userInteractionEnabled = false
                self.demoLayer.addChild(self.colorBubble4)
                
                self.scoreCalculator = ScoreCalculator()
            }),
            SKAction.waitForDuration(1.0),
            SKAction.runBlock({
                self.bombBubble.pop()
            }),
            SKAction.waitForDuration(2.0)
        ]))
        
        demoActions.append(action1)
        demoActions.append(action2)
        demoActions.append(action3)
        demoActions.append(action4)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody:SKPhysicsBody
        var secondBody:SKPhysicsBody
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        // Collission between BombBubble and OTHER bubbles
        if (firstBody.categoryBitMask == BombBubbleCategory || secondBody.categoryBitMask == BombBubbleCategory) {
            if (firstBody.node!.name == "bubble" && secondBody.node!.name == "bubble") {
                var bubble_1 = firstBody.node as! Bubble
                var bubble_2 = secondBody.node as! Bubble
                
                if (bubble_1.isAlive) {
                    if (bubble_1 is ColorBubble) {
                        scoreCalculator.addScore(bubble_1 as! ColorBubble, updatePreColor: false)
                    }
                    bubble_1.pop()
                }
                if (bubble_2.isAlive) {
                    if (bubble_2 is ColorBubble) {
                        scoreCalculator.addScore(bubble_2 as! ColorBubble, updatePreColor: false)
                    }
                    bubble_2.pop()
                }
            }
        }
        // Collision between ColorBubble and PaintBubble
        else if (firstBody.categoryBitMask == ColorBubbleCategory && secondBody.categoryBitMask == PaintBubbleCategory) {
            var colorBubble:ColorBubble = firstBody.node as! ColorBubble
            var paintBubble:PaintBubble = secondBody.node as! PaintBubble
            
            colorBubble.setColor(paintBubble.paintColor!)
            scoreCalculator.prevColor = paintBubble.paintColor!
        }
    }
}