//
//  Boundary.swift
//  BurstYourBubble
//
//  Created by Tianxu Wang on 11/24/14.
//  Copyright (c) 2014 Tianxu Wang. All rights reserved.
//

import SpriteKit

class Boundary : SKSpriteNode {
    
    var pulseAction:SKAction!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(size: CGSize) {
        super.init(texture: nil, color: UIColor.clearColor(), size: size)
        self.name = "colorBoundary"
        self.position = CGPointMake(self.size.width / 2, self.size.height / 2)
        self.zPosition = -1
        self.alpha = 0.8
        
        pulseAction = SKAction.group([
            SKAction.sequence([
                SKAction.fadeAlphaTo(1.0, duration: 0.05),
                SKAction.waitForDuration(0.1),
                SKAction.fadeAlphaTo(0.8, duration: 0.05)
            ]),
            SKAction.sequence([
                SKAction.scaleTo(0.98, duration: 0.05),
                SKAction.waitForDuration(0.1),
                SKAction.scaleTo(1.0, duration: 0.05)
            ])
        ])
    }
    
    func updateTexture(color: BubbleColor) {
        self.texture = colorBoundaryTextures[color.rawValue]
    }
    
    func pulse() {
        self.runAction(pulseAction, withKey: "pulsing")
    }
}
